<?php

require_once "class/hyperobject.php";

class Message extends HyperObject {
	
	public function __construct() {
		$this->constructor();
		$this->addAttr(new HyperAttribute_ID("id"));
		$this->addAttr(new HyperAttribute_String("title"));
		$this->addAttr(new HyperAttribute_Text("message"));
		$this->addAttr(new HyperAttribute_DatetimeStampCreate("datetime_create"));
		$this->addAttr(new HyperAttribute_DatetimeStampModify("datetime_edit"));
		$this->addAttr(new HyperAttribute_Delete("flag_delete"));
		
		$this->init();
	}
}