<?php

require_once "include_classes.php";

class Project {
	public static $name = "HyperObject";
	//Singletons
	public $database_connexion;
	public $menu;
	public $language;
	public $array_entities;
	public $array_plugins;
	public $array_hooks;
	
    public function __construct() {
        //INIT
		$this->database_connexion = new Database_connexion();
        $this->database_connexion->db_connect(DB_NAME);
		
		$this->menu = new Menu();
		$this->menu->add(new MenuItem(array("label" => "Index")));
		
		$this->language = new Language();
		
		$array_entities = array();
		$array_plugins = array();
		$array_hooks = array();
    }
	
	protected function require_once_entities() {
		foreach ($this->array_entities as $entity) {
			require_once("entities/".strtolower($entity).".php");
		}
	}
	
	protected function require_once_plugins() {
		$array_plugins = array_keys($this->array_plugins);
		foreach ($array_plugins as $plugin) {
			require_once("plugins/".strtolower($plugin).".php");
		}
	}
	
	protected function add_menu_entities() {
		foreach ($this->array_entities as $entity) {
			$array_menu = array();
			//Create entity
			$entity_obj = new $entity();
			
			//Get arrays and forms
			
			//array
			$array_menu["label"] = $entity_obj->name." table";
			$array_menu["action"] = "table";
			$array_menu["class"] = $entity_obj->name;
			$this->menu->add(new MenuItem($array_menu));
			
			//form
			$array_menu["label"] = "add ".$entity_obj->name;
			$array_menu["action"] = "form";
			$this->menu->add(new MenuItem($array_menu));
		}
	}
	
	//Install
	public function install(): void {
		//Install database
		if (!$this->database_connexion->db_select(DB_NAME)) {
			$this->database_connexion->db_query("CREATE DATABASE IF NOT EXISTS ".DB_NAME);
			$this->database_connexion->db_select(DB_NAME);
		}
		
		//Install tables
		foreach ($this->array_entities as $entity) {
			//Dynamic version
			$hyperclass = new $entity();
			$hyperclass->install();
			//Static version
			//call_user_func($entity .'::install');
		}
	}
	
	public function uninstall(): void {
		//Drop database
		$this->database_connexion->db_query("DROP DATABASE ".DB_NAME);
	}
	
	/* HOOKS */
	
	public function addHook($action, $class, $method) {
		$this->array_hooks[] = array(
			"action" => $action,
			"class" => $class,
			"method" => $method
		);
	}
	
	/* RENDERING */
	
	public function render() {
		//GET arguments
		$class = "";
		$action = "";
		$template = "";
		$hooked = FALSE;
		
		if (isset($_GET['class'])) {
			$class = clearFormInput($_GET['class']);
		}
		if (isset($_GET['action'])) {
			$action = clearFormInput($_GET['action']);
		}
		if (isset($_GET['template'])) {
			$template = clearFormInput($_GET['template']);
		}
		
		//Init Plugins
		foreach ($this->array_plugins as $plugin => $plugin_obj) {
			$this->array_plugins[$plugin] = new $plugin();
		}
		
		/* BEGIN HTML */
		
		//Start buffer
		ob_start();
		
		//HEAD
		echo Html_skeleton::get_head();
		/* HTML BODY */
		//HEADER
		echo Html_skeleton::get_header();
		//MAIN
		echo '<div id="main">';
		echo Html_skeleton::get_sidebar();
		echo '<div id="main_content">';
		
		//HOOKS
		foreach ($this->array_hooks as $hook) {
			//static method
			$hook_result = $this->array_plugins[$hook["class"]]->{$hook["method"]}();
			if ($hook_result!==TRUE) {
				echo $hook_result;
				$hooked=TRUE;
			}
		}
		
		//Content
		if (!$hooked) {
			if (!empty($action)) {
				if (file_exists("action/".$action.".php")) {
					require_once("action/".$action.".php");
				}
			} else {
				echo '<h1 class="text_center">Bienvenue</h1>';
			}
		}
		
		//End content
		echo '</div>
		</div>';
		//FOOTER
		echo Html_skeleton::get_foot();
		
		//End buffer
		ob_end_flush();
	}
}