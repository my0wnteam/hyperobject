<?php

/**********
***********
**********/
/* HYPER OBJECT PLUGIN */
/**********
***********
**********/

require_once "hyperobject/hyperobject_tableform.php";

abstract class HyperObjectPlugin extends HyperObjectTableForm {
    public function __construct() {
		parent::__construct();
    }

/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
	}
	
	//Override HyperObjectTableForm catchPost
	public function catchPost() {
		//Submit ok
		//Put from POST to attr
		if (isset($_POST['submit_'.$this->name])) {
			foreach ($this->attr as $attr) {
				//Set data in attr
				if (isset($_POST[$attr->name])) {
					//$this->attr[$attr->name]->value = $_POST[$attr->name];
					$this->attr[$attr->name]->set($_POST[$attr->name]);
				}
			}
		}
	}
	
	public function plugin() { 
		//Do what the plugin does
	}
}