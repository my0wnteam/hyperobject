<?php
$db_class = null;

if (isset($_GET)) {
	if (isset($_GET['class'])) {
		$data_class = clearFormInput($_GET['class']);
		$data_id = null;
		if (isset($_GET['data_id'])) {
			$data_id = clearFormInput($_GET['data_id']);
		}
		
		//create class
		$db_class = new $data_class();
		
		//Load data
		if ($data_id) {
			$db_class->load($data_id);
		}
		//Load session data
		if (isset($_SESSION['post_data']['data_class'])) {
			if ($_SESSION['post_data']['data_class'] === $data_class) {
				$db_class->loadSession();
				//Remove session POST data
				unset($_SESSION['post_data']);
			}
		}
	}
}

//RENDER
?>
<h1 class="text_center"><?php echo $GLOBALS['project']->language->get('form'); ?> <?php if ($db_class) { echo $db_class->name; } ?></h1>
<?php
if ($db_class) {
	//Display data array
	//print_r($db_class);
	if (isset($_SESSION['result'])) {
		if ($_SESSION['result']===TRUE) {
			echo "Success";
		} else {
			echo $_SESSION['result'];
		}
		unset($_SESSION['result']);
	}
	
	echo $db_class->getHtmlForm();
}