<?php

class Html_skeleton {
    public function __construct() {
    }
	
	private function __clone() {}
	
	public static function get_head() {
		include "template/head.php";
	}
	
	public static function get_foot() {
		include "template/foot.php";
	}
	
	public static function get_sidebar() {
		//echo self::get_menu();
		include "template/left_menu.php";
	}
	
	public static function get_header() {
		include "template/header.php";
	}
	
	/*public static function get_menu() {
		$html = "";
		
		$html .= '<ul>';
		foreach ($GLOBALS['project']->array_entities as $entity) {
			$html .= '<li><a href="table.php?data_class='.$entity.'">'.$entity.'</a></li>';
		}
		foreach ($GLOBALS['project']->menu->items as $item) {
			$html .= '<li><a href="'.$item.'">'.$item.'</a></li>';
		}
		$html .= '</ul>';
		
		return $html;
	}*/
}