<?php

class DatabaseColumn {
	public $name;
	public $type;
	public $size;
	public $unsigned;
	public $zerofill;
	public $allownull;
	public $defaultvalue;
	public $autoincrement;
	public $extra;
	public $constraint;
	
	private static $type_nolength = "DATE|YEAR|TINYBLOB|MEDIUMBLOB|LONGBLOB|TINYTEXT|LONGTEXT|ENUM|SET|JSON";
	private static $type_length = "BIT|TIME|TIMESTAMP|DATETIME|CHAR|VARCHAR|BINARY|VARBINARY|BLOB|TEXT";
	private static $type_length_uz = "TINYINT|SMALLINT|MEDIUMINT|INT|INTEGER|BIGINT|REAL|DOUBLE|FLOAT|DECIMAL|NUMERIC";
	private static $type_constraint = "PRIMARY KEY|FOREIGN KEY|UNIQUE";

    public function __construct() {
        $this->name = "";
        $this->type = "TEXT";
        $this->size = "";
        $this->unsigned = false;
        $this->zerofill = false;
        $this->allownull = false;
        $this->defaultvalue = "";
		$this->autoincrement = false;
    }
	
	//Use a string to load into a DB column
	public function loadString($string = "") {
		//Get the column infos
		$data_type_nolength = "\b(?P<type>".self::$type_nolength.")\b";
		$data_type_length = "\b(?P<type>".self::$type_length.")\b(?:\s*\((?P<length>.+)\))?";
		$data_type_length_uz = "\b(?P<type>".self::$type_length_uz.")\b(?:\s*\((?P<length>.+)\))?(?:\s+(?P<unsigned>UNSIGNED))?(?:\s+(?P<zerofill>ZEROFILL))?";
		$data_type = $data_type_nolength."|(?:".$data_type_length.")|(?:".$data_type_length_uz.")";
		$pattern = "/^\b(?P<name>\w+)\b\s+(?|".$data_type.")(?:\s+(?P<null>NOT NULL|NULL))?(?:\s+(?P<default>DEFAULT)\s+(?P<default_value>.+))?(?:\s+(?P<autoincrement>AUTO_INCREMENT))?/s";
		//name, type, length, unsigned, zerofill, null, default, default_value
		
		preg_match($pattern, trim($string), $matches);
		
		$this->name = $matches["name"];
		$this->type = $matches["type"];
		if (isset($matches["length"])) {
			$this->size = $matches["length"];
		}
		if (isset($matches["unsigned"])) {
			$this->unsigned = $matches["unsigned"]=="UNSIGNED" ?: false;
		}
		if (isset($matches["zerofill"])) {
			$this->zerofill = $matches["zerofill"]=="ZEROFILL" ?: false;
		}
		if (isset($matches["null"])) {
			$this->allownull = $matches["null"]=="NULL" ?: false;
		}
		if (isset($matches["default"])) {
			$this->defaultvalue = $matches["default_value"];
		}
		if (isset($matches["autoincrement"])) {
			$this->autoincrement = $matches["autoincrement"]=="AUTO_INCREMENT" ?: false;
		}
	}
	
	//Create the string from attributes
	public function getString() {
		$dbString =  $this->name.' '.$this->type;
		if (!empty($this->size)) {
			$dbString .= ' ('.$this->size.') ';
		}
		if ($this->unsigned) {
			$dbString .= ' UNSIGNED ';
		}
		if ($this->zerofill) {
			$dbString .= ' ZEROFILL ';
		}
		if (!$this->allownull) {
			$dbString .= ' NOT NULL ';
		}
		if ($this->defaultvalue != "") {
			$dbString .= ' DEFAULT '.$this->defaultvalue;
		}
		if ($this->autoincrement) {
			$dbString .= ' AUTO_INCREMENT ';
		}
		if (!empty($this->extra)) {
			$dbString .= ' '.$this->extra.' ';
		}
		return trim($dbString);
	}
	
	//Create the constraint from attributes
	public function getConstraintString() {
		$constString = $this->constraint;
		return trim($constString);
	}
	
	//Use a string to create a new DB column
	public static function createFromString($string = "") {
		$DatabaseColumn = new self();
		
		if (!empty($string)) {
			$DatabaseColumn->loadString($string);
		}
		
		return $DatabaseColumn;
	}
	
	public static function createIdFromString($string = "") {
		$DatabaseColumn = DatabaseColumn::createFromString($string);
		$DatabaseColumn->constraint = "PRIMARY KEY (".$DatabaseColumn->name.")";
		
		return $DatabaseColumn;
	}
	
	public static function createFkFromString($string = "", $foreign_table = "", $foreign_key = "") {
		$DatabaseColumn = DatabaseColumn::createFromString($string);
		$DatabaseColumn->constraint = "FOREIGN KEY (".$DatabaseColumn->name.") REFERENCES ".$foreign_table."(".$foreign_key.")";
		
		return $DatabaseColumn;
	}
	
	//test if string is valid column
	public static function testString_type($string = "") {
		$array_needles = explode("|", self::$type_nolength."|".self::$type_length."|".self::$type_length_uz);
		foreach ($array_needles as $needle) {
			if (strpos($string, $needle)!==false) {
				return true;
			}
		}
		return false;
	}
	
	//test if string is constraint
	public static function testString_constraint($string = "") {
		$array_needles = explode ("|", self::$type_constraint);
		foreach ($array_needles as $needle) {
			if (strpos($string, $needle)!==false) {
				return true;
			}
		}
		return false;
	}
}

class DatabaseTable {
	public $name;
	public $columns;
	//public $constraints;
	
	public function __construct() {
		$this->name = "";
		$this->columns = array();
		//$this->constraints = array();
	}
	
	//Use a string to load into a DB Table
	public function loadString($string = "") {
		//Clear columns
		$this->columns = array();
		
		//Get table name and the columns
		$pattern = "/^CREATE\s+(?:TEMPORARY)?\s*TABLE\s+(?:IF NOT EXISTS)?\s*\b(?P<name>\w+)\b\s+\((?P<query>.*)\)/is";
		preg_match($pattern, $string, $matches);
		
		$this->name = $matches["name"];
		
		$array_lines = explode(",", $matches["query"]);
		foreach ($array_lines as $line) {
			if (DatabaseColumn::testString_type($line)) {
				//Create column
				$this->columns[] = DatabaseColumn::createFromString($line);
			} else {
				if (DatabaseColumn::testString_constraint($line)) {
					//Create constraint
				}
			}
		}
	}
	
	//Load HyperAttr into columns
	public function loadAttrs($attrs) {
		//Clear columns
		$this->columns = array();
		
		foreach ($attrs as $attr) {
			//Create column
			$this->columns[] = &$attr->getSqlColumn();
		}
	}
	
	//Create table str
	public function getString() {
		$string = "CREATE TABLE ".$this->name." (";
		
		//columns
		foreach ($this->columns as $column) {
			$string .= $column->getString();
			$string .= ",";
		}
		
		//constraints
		foreach ($this->columns as $column) {
			$cString = $column->getConstraintString();
			if (!empty($cString)) {
				$string .= $cString;
				$string .= ",";
			}
		}
		
		//Remove last , and add )
		$string = substr($string, 0, -1);
		$string .= ")";
		
		//Force InnoDB
		$string .= " ENGINE = InnoDB";
		
		return $string;
	}
	
	public function getColString($col) {
		$string = "ALTER TABLE ".$this->name." ADD ";
		$col_index = $this->findColumnByName($col);
		if ($col_index!==false) {
			$string .= $this->columns[$col_index]->getString();
			
			//position
			if ($col_index===0) {
				$string .= " FIRST";
			} else {
				$string .= " AFTER ".$this->columns[($col_index-1)]->name;
			}
			
			//constraint
			$cString = $this->columns[$col_index]->getConstraintString();
			if (!empty($cString)) {
				$string .= ",".$cString;
			}
		}
		return $string;
	}
	
	public function findColumnByName($name = "") {
		$cpt = 0;
		foreach ($this->columns as $column) {
			if ($column->name===$name) {
				return $cpt;
			}
			$cpt++;
		}
		return false;
	}
}
?>