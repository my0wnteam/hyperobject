<?php

class MenuItem {
	public $label;
	public $template;
	public $action;
	public $class;
	public $args;
	
	public $items = array();
	
	public function __construct($array) {
		if (isset($array["label"])) {
			$this->label = $array["label"];
		} else {
			$this->label = "Undefined";
		}
		
		if (isset($array["template"])) {
			$this->template = $array["template"];
		} else {
			$this->template = "";
		}
		
		if (isset($array["action"])) {
			$this->action = $array["action"];
		} else {
			$this->action = "";
		}
		
		if (isset($array["class"])) {
			$this->class = $array["class"];
		} else {
			$this->class = "";
		}
		
		if (isset($array["args"])) {
			$this->args = $array["args"];
		} else {
			$this->args = array();
		}
		
		//recursive
		if (isset($array["items"])) {
			foreach ($array["items"] as $item) {
				$this->items[] = new MenuItem($item);
			}
		}
	}
	
	public function generateUrl() {
		$args = array();
		
		if (!empty($this->template)) {
			$args["template"] = $this->template;
		}
		if (!empty($this->action)) {
			$args["action"] = $this->action;
		}
		if (!empty($this->class)) {
			$args["class"] = $this->class;
		}
		if (!empty($this->args)) {
			$args = array_merge($args, $this->args);
		}
		
		return generateUrl($args);
		
		/*
		$url = "index.php";
		if (!empty($args)) {
			$url .= "?".http_build_query($args);
		}
		return $url;*/
	}
	
	public function isCurrentPage() {
		if (isset($_GET['template'])) {
			if ($_GET['template'] != $this->template) {
				return FALSE;
			}
		} else {
			if (!empty($this->template)) {
				return FALSE;
			}
		}
		
		if (isset($_GET['action'])) {
			if ($_GET['action'] != $this->action) {
				return FALSE;
			}
		} else {
			if (!empty($this->action)) {
				return FALSE;
			}
		}
		
		if (isset($_GET['class'])) {
			if ($_GET['class'] != $this->class) {
				return FALSE;
			}
		} else {
			if (!empty($this->class)) {
				return FALSE;
			}
		}
		
		return TRUE;
	}
}

class Menu {
	public $items;

    public function __construct() {
        $this->items = array();
    }
	
	public function add($item) {
		if ($item instanceof MenuItem) {
			$this->items[] = $item;
		}
	}
	
	public function loadJSON($json) {
		$array = json_decode($json);
		foreach ($array as $item) {
			$this->items[] = new MenuItem($item);
		}
	}
	
	public function render() {
		$html = "";
		
		$html .= '<ul>';
		
		foreach($this->items as $item) {
			$html .= '<li><a';
			if ($item->isCurrentPage()) {
				$html .= ' class="selected"';
			}
			$html .= ' href="'.$item->generateUrl().'">'.$item->label.'</a></li>';
		}
		
		/*foreach ($GLOBALS['project']->array_entities as $entity) {
			$html .= '<li><a href="table.php?data_class='.$entity.'">'.$entity.'</a></li>';
		}
		foreach ($GLOBALS['project']->menu->items as $item) {
			$html .= '<li><a href="'.$item.'">'.$item.'</a></li>';
		}*/
		$html .= '</ul>';
		
		return $html;
		
		
	}
}