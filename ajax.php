<?php

include 'init.php';

$db_class = NULL;
$action = "";
$handshake_key = "";

$result = array();

if (isset($_GET)) {
	if (isset($_GET['key'])) {
		$handshake_key = clearFormInput($_GET['key']);
		
		if ($handshake_key === API_KEY) {
			if (isset($_GET['data_class'])) {
				$data_class = clearFormInput($_GET['data_class']);
				if (class_exists($data_class)) {
					//create class
					$db_class = new $data_class();
					
					if (isset($_GET['data_action'])) {
						$data_action = clearFormInput($_GET['data_action']);
						$data_params = "";
						
						if (isset($_GET['data_params'])) {
							$data_params = clearFormInput($_GET['data_params']);
						}
						
						//Execute action
						if (!empty($data_params)) {
							//With params
							$result = $db_class->$data_action($data_params);
						} else {
							//Without params
							$result = $db_class->$data_action();
						}
						
					} else {
						//Load data
						$db_class->loadAll();
						$result = $db_class->data;
					}
				} else {
					$result['error'] = "Class does not exist";
				}
			} else {
				$result['error'] = "No data class";
			}
		} else {
			$result['error'] = "Wrong API key";
		}
	} else {
		$result['error'] = "No API key";
	}
}

//CORS
header("Access-Control-Allow-Origin: *");

/* RETURNS JSON */
echo json_encode($result);