<?php

require_once "./class/project.php";

class MyProject extends Project {
	public static $name = "HyperObject Example";
	
    public function __construct() {
        $this->database_connexion = new Database_connexion();
        $this->database_connexion->db_connect(DB_NAME);
		
		$this->menu = new Menu();
		$this->menu->add(new MenuItem(array("label"=>"Index")));
		
		$this->language = new Language();
		
		$this->array_hooks = array();
		
		$this->array_entities = array("User", "Param", "Message", "UserThing");
		//Require entities
		$this->require_once_entities();
		$this->add_menu_entities();
		
		$this->array_plugins = array("PasswordProtect"=>NULL);
		$this->require_once_plugins();
		
		$this->menu->add(new MenuItem(array("label"=>"Install", "action"=>"install")));
		$this->menu->add(new MenuItem(array("label"=>"Test", "action"=>"test")));
    }
}