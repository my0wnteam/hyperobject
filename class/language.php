<?php

class Language {
	public $lang;

    public function __construct() {
        $this->lang = "en";
		
		$this->loadLang();
    }
	
	public function loadLang($lang = "") {
		if (!empty($lang)) {
			$this->lang = $lang;
		}
		
		include "lang/".$this->lang.".php";
	}
	
	public function get($const = "") {
		if (isset($GLOBALS['LANG'][$const])) {
			return $GLOBALS['LANG'][$const];
		} else {
			return $const;
		}
	}
	
	public function getClass($class = "", $const = "") {
		if (isset($GLOBALS['LANG'][strtoupper($class)][$const])) {
			return $GLOBALS['LANG'][strtoupper($class)][$const];
		} else {
			if (isset($GLOBALS['LANG'][$const])) {
				return $GLOBALS['LANG'][$const];
			} else {
				return $const;
			}
		}
	}
}
?>
