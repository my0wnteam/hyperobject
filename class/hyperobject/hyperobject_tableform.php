<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperobject_data.php";

abstract class HyperObjectTableForm extends HyperObjectData {
	//Structures generated
	public $html_tables;
	public $html_forms;
	
	public function __construct() {
		parent::__construct();
		$this->html_tables = array();
		$this->html_forms = array();
    }
	
/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
	}

	
	//Put in attr
	public function catchPost() {
		//Submit ok
		//Put from POST to attr
		if (isset($_POST['submit_'.$this->name])) {
			foreach ($this->attr as $attr) {
				//Set data in attr
				if (isset($_POST[$attr->name])) {
					//$this->attr[$attr->name]->value = $_POST[$attr->name];
					$this->attr[$attr->name]->set($_POST[$attr->name]);
				}
			}
			
			//New or update ?
			if (!empty($this->attr[$this->getPrimaryKey()->name]->get())) {
				//Place pointer
				if ($this->setPtrId($this->attr[$this->getPrimaryKey()->name]->get())) {
					$this->attrToData();
				} else {
					$this->attrNewData();
				}
			} else {
				$this->attrNewData();
			}
		}
	}
	
	//Used in post.php
	public function errorCheck() {
		foreach ($this->attr as $attr) {
			if ($attr->error !== FALSE) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/* LOAD SESSION POSTDATA */
	
	public function loadSession() {
		//Reload POST data into attr
		foreach ($this->attr as $attr) {
			if (isset($_SESSION['post_data'][$attr->name])) {
				//$attr->value = $_SESSION['post_data'][$attr->name];
				$attr->set($_SESSION['post_data'][$attr->name]);
			}
		}
	}
	
	/* HTML FORM */
	
	public function addHtmlForm($array_form) {
		if (isset($array_form["name"])) {
			$this->html_forms[$array_form["name"]] = $array_form;
		} else {
			//Store but unreachable
			$this->html_forms[] = $array_form;
		}
	}
	
/** Generate a delete form in HTML
* @param id {int} ID to delete
* @return {string} HTML form
**/
	public function getHtmlDeleteForm($id = 0) {
		$html = "";
		$html .= '<form action="delete.php" method="post" class="inline_block">';
		
		//submit button
		$html .= '<input type="submit" id="submit_'.$this->name.'" name="submit_'.$this->name.'" value="Delete" />';
		
		//hidden class
		$html .= '<input type="hidden" id="data_class" name="data_class" value ="'.$this->name.'" />';
		
		//hidden ID
		$html .= '<input type="hidden" id="data_id" name="data_id" value="'.$id.'" />';
		
		//hidden page source
		$html .= '<input type="hidden" id="source_page" name="source_page" value="'.$_SERVER['REQUEST_URI'].'" />';
		
		$html .= '</form>';
		
		return $html;
	}
	
	/* BASIC HTML FORM */
	//From attr value
	
	//Form to add, edit
	public function getHtmlForm($name = "") {
		$html_form = new Html_form();
		$html_form->setHyperClass($this);
		
		foreach ($this->attr as $attr) {
			if (!($attr->flags & HIDDEN_FORM)) {
				if (!empty($name)) {
					//There is a form selected
					if (isset($this->html_forms[$name])) {
						if (in_array($attr->name, $this->html_forms[$name]["attr"])) {
							$html_form->addField($attr->getHtml(), $attr->form_style, $attr->getClassLabel($this->name));
						}
					} else {
						//Stop foreach loop
						break;
					}
				} else {
					$html_form->addField($attr->getHtml(), $attr->form_style, $attr->getClassLabel($this->name));
				}
			}
		}
		
		return $html_form->generateHtml();
	}
	
	/* HTML TABLE */
	
	public function addHtmlTable($array_table) {
		if (isset($array_table["name"])) {
			$this->html_tables[$array_table["name"]] = $array_table;
		} else {
			//Store but unreachable
			$this->html_tables[] = $array_table;
		}
	}
	
	//Array
	//From data
	public function getHtmlTable($name = "") {
		$html_table = Html_table::createWithPtr($this);
		
		if (!empty($name)) {
			//There is a table model as parameter
			if (isset($this->html_tables[$name])) {
				//Table model exists
				$array_columns = array_keys($this->html_tables[$name]["attr"]);
				
				foreach ($this->attr as $attr) {
					if (in_array($attr->name, $array_columns)) {
						//In array
						$html_table->addColumn($attr, !($this->html_tables[$name]["attr"][$attr->name] & NO_SEARCH_TABLE), !($this->html_tables[$name]["attr"][$attr->name] & NO_FILTER_TABLE), !($this->html_tables[$name]["attr"][$attr->name] & NO_SORT_TABLE));
					}
				}
			}
		} else {
			//Default
			foreach ($this->attr as $attr) {
				if (!($attr->flags & HIDDEN_TABLE)) {
					$html_table->addColumn($attr, !($attr->flags & NO_SEARCH_TABLE), !($attr->flags & NO_FILTER_TABLE), !($attr->flags & NO_SORT_TABLE));
				}
			}
		}
		
		return $html_table->generateHtml();
	}
	
	public function getHtmlButton() {
		//return '<a class="button" href="form.php?data_class='.$this->name.'">'."New".'</a>';
		return '<a class="button" href="'.generateUrl(array("action"=>"form", "class"=>$this->name)).'">'.$GLOBALS['project']->language->get('new')." ".$this->name.'</a>';
	}
}