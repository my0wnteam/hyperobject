<?php

include 'init.php';

if (isset($_POST)) {
	if (isset($_POST['filter'])) {
		$attr = $_POST['filter_attr'];
		$value = $_POST[$attr];
		
		//POST -> GET
		header("Location: ".UrlQueryRebuild(array("filter" => $attr, "filter_value" => $value)));
		die;
	}
}

$db_class = NULL;
$table_name = "";
if (isset($_GET)) {
	if (isset($_GET['data_class'])) {
		$data_class = clearFormInput($_GET['data_class']);
			
		//create class
		$db_class = new $data_class();
		//Load data
		if (isset($_GET['filter']) && isset($_GET['filter_value'])) {
			//filter
			$filter = clearFormInput($_GET['filter']);
			$filter_value = clearFormInput($_GET['filter_value']);
			
			if (!empty($filter_value)) {
				$db_class->loadQuery("SELECT * FROM ".$db_class->name." WHERE ".$filter."='".$filter_value."' ORDER BY ".$db_class->getPrimaryKey()->name);
			} else {
				$db_class->loadAll();
			}
		} else {
			$db_class->loadAll();
		}
	}
	if (isset($_GET['table_name'])) {
		$table_name = clearFormInput($_GET['table_name']);
	}
	if (isset($_GET['sort'])) {
		//Sort data
		$GLOBALS['sort_attr'] = clearFormInput($_GET['sort']);
		
		if (isset($_GET['sort_order'])) {
			if ($_GET['sort_order']==="desc") {
				usort($db_class->data, function($a,$b){return strcmp($b[$GLOBALS['sort_attr']], $a[$GLOBALS['sort_attr']]);});
			} else {
				usort($db_class->data, function($a,$b){return strcmp($a[$GLOBALS['sort_attr']], $b[$GLOBALS['sort_attr']]);});
			}
		} else {
			usort($db_class->data, function($a,$b){return strcmp($a[$GLOBALS['sort_attr']], $b[$GLOBALS['sort_attr']]);});
		}
	}
}
?>
<?php Html_skeleton::get_head(); ?>
<?php Html_skeleton::get_header(); ?>
	<div id="main">
	<?php echo Html_skeleton::get_sidebar(); ?>
		<div id="main_content">
<h1 class="text_center">Array <?php if ($db_class) { echo $db_class->name; } ?></h1>
<?php
if ($db_class) {
	//Display data array
	echo $db_class->getHtmlButton();
	echo $db_class->getHtmlArray($table_name);
}
?>
		</div>
	</div>
<?php Html_skeleton::get_foot(); ?>