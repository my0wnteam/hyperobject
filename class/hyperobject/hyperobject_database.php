<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperobject_tableform.php";

abstract class HyperObjectDatabase extends HyperObjectTableForm {
	public function __construct() {
		parent::__construct();
    }
	
/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
	}
	
	/* LOAD AND SAVE */
	//Work with data array
	
	//Get multiple objects
	public function loadAll() {
		$query = "SELECT * FROM ".$this->name;
		//If there is a booldelete attr, add it as condition
		if ($booldelete = $this->searchAttr("booldelete")) {
			$query .= " WHERE ".$booldelete->name."=0";
		}
		$query .= " ORDER BY ".$this->getPrimaryKey()->name;
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		if ($result === false) {
			echo $GLOBALS['project']->database_connexion->db_error();
		} else {
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$this->data[] = $row;
			}
		}
	}
	
	//Get query objects
	public function loadQuery($query = "") {
		//TO REDO
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		if ($result === false) {
			echo $GLOBALS['project']->database_connexion->db_error();
		} else {
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$this->data[] = $row;
			}
		}
	}
	
	//Get single object
	public function load($id) {
		$query = "SELECT * FROM ".$this->name." WHERE ".$this->getPrimaryKey()->name."=?";
		//If there is a booldelete attr, add it as condition
		if ($booldelete = $this->searchAttr("booldelete")) {
			$query .= " AND ".$booldelete->name."=0";
		}
		$result = $GLOBALS['project']->database_connexion->db_query($query, array($id));
		
		if ($result === false) {
			echo $GLOBALS['project']->database_connexion->db_error();
		} else {
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$this->data[$id] = $row;
			}
			
			//place pointer on last data inserted
			$this->setPtr($id);
		}
	}
	
	//Save to database
	public function save($ptr = null) {
		//Place pointer
		$this->setPtr($ptr);
		
		$query = "";
		$query_cols = "";
		$query_vals = "";
		$query_key_val = "";
		
		foreach ($this->attr as $attr) {
			//if ($attr->type != "id") {
			if ($attr->flags & RIGHT_WRITE) {
				if (isset($this->data[$this->data_ptr][$attr->name])) {
					$query_cols .= $attr->name.",";
					$query_vals .= "'".addslashes($this->data[$this->data_ptr][$attr->name])."',";
					
					$query_key_val .= $attr->name."='".addslashes($this->data[$this->data_ptr][$attr->name])."',";
				}
			}
		}
		//Remove last ,
		$query_cols = substr($query_cols, 0, -1);
		$query_vals = substr($query_vals, 0, -1);
		$query_key_val = substr($query_key_val, 0, -1);
		
		/* build query */
		if ($this->data[$this->data_ptr][$this->getPrimaryKey()->name]) {
			//Primary key exists = update
			$query = "UPDATE ".$this->name." SET ";
			
			//key = value
			$query .= $query_key_val;
			
			//where
			$query .= " WHERE ".$this->getPrimaryKey()->name."=".$this->data[$this->data_ptr][$this->getPrimaryKey()->name];
		} else {
			//create
			$query = "INSERT INTO ".$this->name;
			//columns
			$query .= " (";
			$query .= $query_cols;
			$query .= ")";
			//values
			$query .= " VALUES (";
			$query .= $query_vals;
			$query .= ")";
		}
		
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		if ($result === false) {
			return $GLOBALS['project']->database_connexion->db_error();
		} else {
			return $result;
		}
	}
	
	public function saveAll() {
		//Begin transaction
		$GLOBALS['project']->database_connexion->db_transaction();
		
		//Stack queries
		for ($i=0; $i<count($this->data); $i++) {
			$this->save($i);
		}
		
		//End transaction
		$GLOBALS['project']->database_connexion->db_commit();
	}
	
	/* DELETE DATA */
	
	public function deleteData($id) {
		if ($booldelete = $this->searchAttr("booldelete")) {
			//Delete attr
			$query = "UPDATE ".$this->name." SET ".$booldelete->name."=1 WHERE ".$this->getPrimaryKey()->name."=".$id;
		} else {
			//No delete attr
			$query = "DELETE FROM ".$this->name." WHERE ".$this->getPrimaryKey()->name."=".$id;
		}
		
		$result = $GLOBALS['project']->database_connexion->db_query($query);
			
		if ($result === false) {
			return $GLOBALS['project']->database_connexion->db_error();
		} else {
			return $result;
		}
	}
}