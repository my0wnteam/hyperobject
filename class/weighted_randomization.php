<?php

class WeightedRandomization {
	public $items;

    public function __construct() {
        $this->items = array();
    }
	
	public function add($item, $important = false) {
		$item_container = array();
		$item_container['item'] = $item;
		$item_container['weight'] = $important;
		
		$this->items[] = $item_container;
	}
	
	private function calcWeight() {
		$weight = 0;
		foreach ($this->items as &$item) {
			$weight++;
			if ($item['weight']) {
				$weight += count($this->items);
			}
			$item['max'] = $weight;
		}
		return $weight;
	}
	
	public function getRandom() {
		$weight = $this->calcWeight();
		$random = rand(1, $weight);	
		
		foreach ($this->items as $item) {
			if ($random <= $item['max']) {
				
				return $item['item'];
			}
		}
	}
}

?>