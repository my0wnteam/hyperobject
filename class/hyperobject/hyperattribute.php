<?php

define("RIGHT_READ", 1);
define("RIGHT_WRITE", 2);
define("HIDDEN_TABLE", 4);
define("HIDDEN_FORM", 8);
define("NO_SORT_TABLE", 16);
define("NO_SEARCH_TABLE", 32);
define("NO_FILTER_TABLE", 64);

require_once "class/project.php";

/**********
***********
**********/
/* HYPER ATTRIBUTE */
/**********
***********
**********/

abstract class HyperAttribute {
	public $name;
	public $value;
	public $error;
	public $type;
	public $default_value;
	public $flags; //RIGHT_READ|RIGHT_WRITE|HIDDEN_TABLE|HIDDEN_FORM|NO_SORT_TABLE|NO_SEARCH_TABLE|NO_FILTER_TABLE
	public $database_column;
	public $form_style; //from html_form.php
	
	public function set($val) {
		$this->value = $val;
		if (!$this->isRightType($this->value)) {
			$this->error = "Wrong type: ".gettype($this->value)." Value: ".$this->value;
		} else {
			$this->error = FALSE;
		}
	}
	
	public function get() {
		return $this->value;
	}
	
	public function displayValue() {
		return $this->value;
	}
	
	abstract function isRightType($val);
	
	abstract function getHtml(); //form + label + style
	
	public function getLabel() {
		return '<label for="'.$this->name.'">'.$this->name.'</label>';
	}
	
	public function getClassName($class) {
		return $GLOBALS['project']->language->getClass($class, $this->name);
	}
	
	public function getClassLabel($class) {
		return '<label for="'.$this->name.'">'.$this->getClassName($class).'</label>';
	}
	
	//abstract function getPhp();
	
	public function getSql() {
		$this->database_column->getString();
	}
	
	public function &getSqlColumn() {
		return $this->database_column;
	}
	
	public function install_checkDbCol($table) {
		$query = "SHOW COLUMNS FROM ".$table." LIKE '".$this->name."'";
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		return $result->num_rows;
	}
}

/* ID */

class HyperAttribute_ID extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "id";
		$this->default_value = NULL;
		$this->flags = RIGHT_READ|NO_SORT_TABLE|NO_FILTER_TABLE;
		$this->database_column = DatabaseColumn::createIdFromString($this->name.' INT (11) UNSIGNED NOT NULL AUTO_INCREMENT');
		
		$this->form_style = "STYLE_NONE_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="hidden" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* STRING */

class HyperAttribute_String extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "string";
		$this->default_value = "";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (255) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_string($val);
	}
	
	public function getHtml() {
		$html = '<input type="text" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Text extends HyperAttribute_String {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "text";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' TEXT NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function getHtml() {
		$html = '<textarea id="'.$this->name.'" name="'.$this->name.'" placeholder="">'.$this->get().'</textarea>';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
	
	public function displayValue() {
		return nl2br($this->value);
	}
}

/* EMAIL STRING */

class HyperAttribute_Email extends HyperAttribute_String {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "email";
		$this->default_value = "";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (255) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		//return is_string($val);
		return is_email($val);
	}
	
	public function getHtml() {
		$html = '<input type="email" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* EMAIL STRING */

class HyperAttribute_Password extends HyperAttribute_String {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "password";
		$this->default_value = "";
		$this->flags = RIGHT_READ|RIGHT_WRITE|HIDDEN_TABLE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (255) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function getHtml() {
		$html = '<input type="password" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* COLOR STRING */

class HyperAttribute_Color extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "color";
		$this->default_value = "";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (16) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_string($val);
	}
	
	public function getHtml() {
		$html = '<input type="color" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
	
	public function displayValue() {
		return '<div style="background-color:'.$this->value.'; width: 1em; height: 1em; border-radius: 50%;"></div>';
	}
}

/* INT */

class HyperAttribute_Int extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "int";
		$this->default_value = 0;
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (11) NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="number" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* YES NO */

class HyperAttribute_YesNo extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 1;
		$this->error = FALSE;
		$this->type = "int";
		$this->default_value = 1;
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (1) NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getClassLabel($class) {
		return '<div class="label">'.$GLOBALS['project']->language->getClass($class, $this->name).'</div>';
	}
	
	public function getHtml() {
		$html = '<input type="radio" id="'.$this->name.'_yes" name="'.$this->name.'" value="1"';
		if ($this->get()) {
			//Value is unlike empty
			$html .= ' checked ';
		}
		$html .= '/><label for="'.$this->name.'_yes" class="no_style margin_left_10px margin_right_50px">'.$GLOBALS['project']->language->get('yes').'</label>';
		$html .= '<input type="radio" id="'.$this->name.'_no" name="'.$this->name.'" value="0"';
		if (!$this->get()) {
			//Value is like empty
			$html .= ' checked ';
		}
		$html .= '/><label for="'.$this->name.'_no" class="no_style margin_left_10px margin_right_50px">'.$GLOBALS['project']->language->get('no').'</label>';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
	
	public function displayValue() {
		return ($this->value) ? $GLOBALS['project']->language->get('yes') : $GLOBALS['project']->language->get('no');
	}
}

/* DATETIME */

class HyperAttribute_Datetime extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetime";
		$this->default_value = date('Y-m-d H:i:s');
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return TRUE; //is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="datetime-local" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* CREATE DATETIME */

class HyperAttribute_DatetimeStampCreate extends HyperAttribute_Datetime {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetimestamp_create";
		$this->default_value = date('Y-m-d H:i:s');
		$this->flags = RIGHT_READ|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
		
		$this->form_style = "STYLE_INPUT";
	}
}

/* MODIFY DATETIME */

class HyperAttribute_DatetimeStampModify extends HyperAttribute_Datetime {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetimestamp_modify";
		$this->default_value = date('Y-m-d H:i:s');
		$this->flags = RIGHT_READ|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
		//Extra trigger
		$this->database_column->extra = "ON UPDATE CURRENT_TIMESTAMP";
		
		$this->form_style = "STYLE_INPUT";
	}
}

/* BOOL */

class HyperAttribute_Bool extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "bool";
		$this->default_value = 0;
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (1) UNSIGNED NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_SPACE_INPUT_LABEL";
	}
	
	//toBool
	public function set($val) {
		$this->value = toBool($val);
		if (!$this->isRightType($this->value)) {
			$this->error = "Wrong type: ".gettype($this->value)." Value: ".$this->value;
		} else {
			$this->error = FALSE;
		}
	}
	
	public function isRightType($val) {
		return is_bool($val);
	}
	
	public function getHtml() {
		$html = '<input type="checkbox" id="'.$this->name.'" name="'.$this->name.'"';
		if ($this->get()) {
			$html .= ' checked';
		}
		$html .= ' />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

/* DELETE BOOL */

class HyperAttribute_Delete extends HyperAttribute_Bool {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "booldelete";
		$this->default_value = 0;
		$this->flags = RIGHT_READ|HIDDEN_TABLE|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (1) UNSIGNED NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_SPACE_INPUT_LABEL";
	}
}

/* FOREIGN KEY */

class HyperAttribute_ForeignKey extends HyperAttribute {
	private $foreign_table;
	private $foreign_key;
	private $foreign_display_field;
	
	public function __construct($name, $foreign_table, $foreign_key, $foreign_display_field="") {
		$this->name = $name;
		$this->foreign_table = $foreign_table;
		$this->foreign_key = $foreign_key;
		$this->foreign_display_field = $foreign_display_field;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "id";
		$this->default_value = NULL;
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFkFromString($this->name.' INT (11) UNSIGNED NOT NULL', $this->foreign_table, $this->foreign_key);
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<select id="'.$this->name.'" name="'.$this->name.'">';
		
		//Create foreign object
		$foreign_object = new $this->foreign_table();
		$foreign_object->loadAll();
		
		foreach ($foreign_object->data as $data_row) {
			$html .= '<option value="'.$data_row[$this->foreign_key].'"';
			if ($this->get()===$data_row[$this->foreign_key]) {
				$html .= ' selected';
			}
			$html .= '>';
			if (!empty($this->foreign_display_field)) {
				$html .= $data_row[$this->foreign_display_field];
			} else {
				$html .= $data_row[$this->foreign_key];
			}
			$html .= '</option>';
		}
		
		$html .= '</select>';
		
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
	
	public function displayValue() {
		if (!empty($this->foreign_display_field)) {
			$foreign_object = new $this->foreign_table();
			$foreign_object->load($this->value);
			return $foreign_object->attr[$this->foreign_display_field]->get();
		} else {
			return $this->value;
		}
	}
}
