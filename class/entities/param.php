<?php

require_once "class/hyperobject.php";

class Param extends HyperObject {
	public function __construct() {
		$this->constructor();
		$this->addAttr(new HyperAttribute_ID("id_param"));
		$this->addAttr(new HyperAttribute_String("param_name"));
		$this->addAttr(new HyperAttribute_String("param_value"));
		$this->addAttr(new HyperAttribute_DatetimeStampCreate("datetime_create"));
		$this->addAttr(new HyperAttribute_DatetimeStampModify("datetime_edit"));
		$this->addAttr(new HyperAttribute_Delete("flag_delete"));
		$this->init();
	}
}