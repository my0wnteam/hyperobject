<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperobject/hyperobject_database_install.php";

abstract class HyperObject extends HyperObjectDatabaseInstall {
    public function __construct() {
		parent::__construct();
    }

/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
		
		$this->database_table = new DatabaseTable();
		$this->database_table->name = $this->name;
	}
}