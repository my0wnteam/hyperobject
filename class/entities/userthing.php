<?php

require_once "class/hyperobject.php";

class UserThing extends HyperObject {
	
	public function __construct() {
		$this->constructor();
		$this->addAttr(new HyperAttribute_ID("id_user"));
		$this->addAttr(new HyperAttribute_String("user_string"));
		$this->addAttr(new HyperAttribute_ForeignKey("user_thing", "user", "id_acc", "acc_email"));
		$this->addAttr(new HyperAttribute_DatetimeStampCreate("datetime_create"));
		$this->addAttr(new HyperAttribute_DatetimeStampModify("datetime_edit"));
		$this->addAttr(new HyperAttribute_Delete("flag_delete"));
		
		$this->init();
	}
}