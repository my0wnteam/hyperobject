<?php

define("STYLE_LABEL_INPUT", "<div>[label][input]</div>");
define("STYLE_INPUT_LABEL", "<div>[input][label]</div>");
define("STYLE_INPUT", "<div>[input]</div>");
define("STYLE_SPACE_INPUT_LABEL", '<div><div class="empty_label"></div>[input][label]</div>');
define("STYLE_NONE_INPUT", '[input]');

class Html_form_input {
	public $style;
	public $label;
	public $input;
	
	public function __construct() {
        $this->style = "STYLE_INPUT";
        $this->label = "";
        $this->input = "";
    }
	
	public static function createWithParams($input, $style, $label) {
		$Form_input = new self();
		
		$Form_input->input = $input;
		$Form_input->style = $style;
		$Form_input->label = $label;
		
		return $Form_input;
	}
	
	function generateHtml() {
		$html = constant($this->style);
		
		return str_replace(
			array("[label]", "[input]"),
			array($this->label, $this->input),
			$html
		);
	}
}

class Html_form {
	//public $data;
	
	public $header;
	public $method;
	public $action;
	public $css_classes;
	
	public $footer;
	public $fields;
	
	private $hyperclass_ptr;

    public function __construct() {
        //$this->data = NULL;
		$this->method = "POST";
		$this->action = "post.php";
		$this->css_classes = "form_style";
		$this->header = '<form action="post.php" method="post" class="form_style">';
		$this->footer = '</form>';
		$this->hyperclass_ptr = NULL;
    }
	
	public function setHyperClass($hyperclass) {
		$this->hyperclass_ptr = &$hyperclass;
	}
	
	private function generateHtmlHeader() {
		$this->header = '<form action="'.$this->action.'" method="'.$this->method.'" class="'.$this->css_classes.'">';
	}
	
	public function generateHtml() {
		$this->generateHtmlHeader();
		$html = $this->header;
		
		//Form fields
		foreach ($this->fields as $field) {
			$html .= $field->generateHtml();
		}
		
		//Submit button
		if ($this->hyperclass_ptr) {
			$html .= '<input type="submit" class="button" id="submit_'.$this->hyperclass_ptr->name.'" name="submit_'.$this->hyperclass_ptr->name.'" value="'.$GLOBALS['project']->language->get('save').'" />';
			
			//Hidden class
			$html .= '<input type="hidden" id="data_class" name="data_class" value ="'.$this->hyperclass_ptr->name.'" />';
		} else {
			$html .= '<input type="submit" class="button" name="submit" value="'.$GLOBALS['project']->language->get('save').'" />';
		}
		
		//Hidden page source
		$html .= '<input type="hidden" id="source_page" name="source_page" value="'.$_SERVER['REQUEST_URI'].'" />';
		
		$html .= $this->footer;
		return $html;
	}
	
	public function addField($input, $style="STYLE_INPUT", $label="") {
		$this->fields[] = Html_form_input::createWithParams($input, $style, $label);
	}
	
	public function clearFields() {
		foreach ($this->fields as $field) {
			unset($field);
		}
	}
}