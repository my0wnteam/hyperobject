<?php

require_once "class/hyperobject.php";

class User extends HyperObject {
	/*
	public static $query = "CREATE TABLE t_account (
	id_acc INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
	acc_email VARCHAR (255) NOT NULL,
	acc_pwd CHAR (32) NOT NULL,
	acc_name VARCHAR (255) NOT NULL,
	acc_superadmin INT (1) UNSIGNED NOT NULL,
	acc_token TEXT NOT NULL,
	acc_active INT (1) UNSIGNED NOT NULL,
	acc_delete INT (1) UNSIGNED NOT NULL,
	acc_lastvisit DATETIME NOT NULL,
	date_create DATETIME NOT NULL,
	date_edit DATETIME NOT NULL,
	PRIMARY KEY (id_acc),
	UNIQUE (acc_email)
	)";
	*/
	
	public function __construct() {
		$this->constructor();
		$this->addAttr(new HyperAttribute_ID("id_acc"));
		$this->addAttr(new HyperAttribute_Email("acc_email"));
		$this->addAttr(new HyperAttribute_String("acc_name"));
		$this->addAttr(new HyperAttribute_String("acc_fname"));
		$this->addAttr(new HyperAttribute_Int("acc_number"));
		$this->addAttr(new HyperAttribute_Bool("acc_active"));
		$this->addAttr(new HyperAttribute_DatetimeStampCreate("datetime_create"));
		$this->addAttr(new HyperAttribute_DatetimeStampModify("datetime_edit"));
		$this->addAttr(new HyperAttribute_Delete("flag_delete"));
		
		$this->addHtmlTable(array("name" => "test", "attr"=>array(
			"acc_email" => NO_FILTER_TABLE,
			"id_acc" => NO_SORT_TABLE|NO_FILTER_TABLE,
			"acc_name" => NO_FILTER_TABLE
		)));
		
		$this->init();
	}
}