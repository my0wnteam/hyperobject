<?php
/*******************************************************************************
User
*******************************************************************************/
class User {
    public $id = 0;
    public $name;
    public $displayName;
    public $password;
    public $email;
    public $created;
    public $enabled;
    public $roles = array();
    public $company;

    private $errorCode = false;

    private $dbLink;
    private $gTool;
    private $gLog;


    /***************************************************************************
    constructor
    ***/
    public function __construct() {
        $this->dbLink = $GLOBALS['gDb'];
        $this->gTool  = $GLOBALS['gTool'];
        $this->gLog   = $GLOBALS['gLog'];
    }


    /***************************************************************************
    getById
    Returns a user based on its id in database.
    ***/
    public function getById($id) {

        $res = $this->dbLink->quickFetch("
            SELECT user_id
                ,user_name
                ,user_displayName
                ,user_password
                ,user_email
                ,user_created
                ,user_enabled
                ,user_company
            FROM t_user
            WHERE user_id = :id
        ", array(':id'=>$id));

        if(is_array($res)) {
            $this->id          = $res['user_id'];
            $this->name        = $res['user_name'];
            $this->displayName = $res['user_displayName'];
            $this->password    = $res['user_password'];
            $this->email       = $res['user_email'];
            $this->created     = $res['user_created'];
            $this->enabled     = $res['user_enabled'];
            $this->company     = $res['user_company'];
        }

        if($this->id == 0) {
            $this->errorCode = 'USER_NOT_EXIST';
            return false;
        }

        // get roles
        $res = $this->dbLink->quickFetchAll("
            SELECT userUserRole_role
            FROM t_userUserRole
            WHERE userUserRole_user = :id
        ", array(':id'=>$id));

        if(is_array($res)) {
            $this->roles = array();
            foreach($res as $row) {
                $this->roles[] = $row['userUserRole_role'];
            }
        }

        return $this;
    }


    /***************************************************************************
    create
    Adds a new user to database
    ***/
    public function create($user) {

        // check input
        if(!is_array($user)) {
            $this->errorCode = 'NOT_AN_ARRAY';
            return false;
        }
        elseif(empty($user['name'])) {
            $this->errorCode = 'USER_NAME_MISSING';
            return false;
        }
        elseif(empty($user['password'])) {
            $this->errorCode = 'PASSWORD_MISSING';
            return false;
        }
        elseif(empty($user['passwordConfirm'])) {
            $this->errorCode = 'PASSWORD_CONFIRMATION_MISSING';
            return false;
        }
        elseif($user['passwordConfirm'] != $user['password']) {
            $this->errorCode = 'PASSWORD_MISMATCH';
            return false;
        }
        elseif(empty($user['email'])) {
            $this->errorCode = 'USER_MAIL_MISSING';
            return false;
        }
        elseif(empty($user['company'])) {
            $this->errorCode = 'USER_COMPANY_MISSING';
            return false;
        }

        // default value (in case create function is called without the proper form)
        $default = array(
            'displayName' => ''
            ,'enabled'    => 1
            ,'company'    => ''
        );

        $user = array_merge($default, $user);


        // new db instance
        $link = $this->dbLink->create();

        $reqsql_add_user = "INSERT INTO t_user
            (user_name, user_email, user_displayName, user_created, user_enabled, user_company)
            VALUES
            (:name, :email, :display, NOW(), :enabled, :company)";

        $link
            ->open()
            ->prep($reqsql_add_user)
            ->bindArray(array(
                ':name'     => $user['name']
                ,':email'   => $user['email']
                ,':display' => $user['displayName']
                ,':enabled' => $user['enabled'] ? 1 : 0
                ,':company' => $user['company']
            ))
            ->exec();

        if(!$link->success()) {
            $this->errorCode = 'USER_CREATION_FAILURE';
            return false;
        }

        $reqsql_add_user = "SELECT user_id
            ,user_created
            FROM t_user
            WHERE user_id = LAST_INSERT_ID()";

        $ressql_add_user = $link
            ->prep($reqsql_add_user)
            ->exec()
            ->fetch();

        if(!is_array($ressql_add_user)) {

            $this->errorCode = 'USER_CREATION_FAILURE';
            return false;
        }

        $reqsql_add_user = "UPDATE t_user
            SET user_password = :pwd
            WHERE user_id = LAST_INSERT_ID()";

        $link
            ->bindClear()
            ->prep($reqsql_add_user)
            ->bindArray(array(':pwd'=>$this->gTool
                ->passwordHash($user['password'], $ressql_add_user['user_created'])))
            ->exec()
            ->close();

        if($this->getById($ressql_add_user['user_id']) == false) {
            return false;
        }

        // add roles

        if( isset($user['roles']) || is_null($user['roles']) ) {
            $this->setRoles($user['roles']);
        }

        // update log
        $this->gLog->updateUser($this->id);

        return true;
    }


    /***************************************************************************
    enableById
    Enables a user based on its Id
    ***/
    static public function enableById($id) {

        $reqsql_enable = "UPDATE t_user
            SET user_active = 1
            WHERE user_id = :id";

            $this->dbLink
                ->quickExec($reqsql_enable, array(':id'=>$id));

        return $this;
    }


    /***************************************************************************
    enable
    Enables the current user iteration
    ***/
    public function enable() {

        $this->enableById($this->id);

        return $this;
    }


    /***************************************************************************
    disableById
    Disables a user based on its Id
    ***/
    static public function disableById($id) {


        $reqsql_disable = "UPDATE t_user
            SET user_active = 0
            WHERE user_id = :id";

            $this->dbLink
                ->quickExec($reqsql_disable, array(':id'=>$id));

        return $this;
    }


    /***************************************************************************
    disable
    Disables the current user iteration
    ***/
    public function disable() {

        $this->disableById($this->id);

        return $this;
    }


    /***************************************************************************
    edit
    Edits the current user iteration
    ***/
    public function edit($user) {

        $reqsql = array();
        $bind = array();
        //$link = $this->dbLink->create();

        // check input
        if($this->id == 0) {
            $this->errorCode = 'USER_NOT_EXIST';
            return false;
        }

        if(!empty($user['name'])) {
            $reqsql[] = ' user_name = :name';
            $bind[':name'] = $user['name'];
        }
        else {
            $this->errorCode = 'USER_NAME_MISSING';
            return false;
        }

        if(!empty($user['password'])) {

            if($user['password'] == $user['passwordConfirm']) {
                $reqsql[] = ' user_password = :pwd';
                $bind[':pwd'] = $this->gTool->passwordHash($user['password'], $this->created);
            }
            else {
                $this->errorCode = 'PASSWORD_MISMATCH';
                return false;
            }
        }

        if(!empty($user['email'])) {

            if(filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {
                $reqsql[] = ' user_email = :email';
                $bind[':email'] = $user['email'];
            }
            else {
                $this->errorCode = 'USER_MAIL_INCORRECT';
                return false;
            }
        }
        else {
            $this->errorCode = 'USER_MAIL_MISSING';
            return false;
        }

        if(!empty($user['company'])) {
            $reqsql[] = ' user_company = :company';
            $bind[':company'] = $user['company'];
        }
        else {
            $this->errorCode = 'USER_COMPANY_MISSING';
            return false;
        }

        if( isset($user['displayName']) || is_null($user['displayName']) ) {
            $reqsql[] = ' user_displayName = :displayName';
            $bind[':displayName'] = $user['displayName'];
        }

        if( isset($user['enabled']) || is_null($user['enabled']) ) {
            $reqsql[] = ' user_enabled = :enabled';
            $bind[':enabled'] = $user['enabled'];
        }

        // update user info
        $bind[':id'] = $this->id;
        $link = $this->dbLink->quickExec("
            UPDATE t_user
            SET ".implode(',', $reqsql)."
            WHERE user_id = :id
        ", $bind);

        if(!$link->success()) {
            return false;
        }

        if( isset($user['roles']) || is_null($user['roles']) ) {
            $this->setRoles($user['roles']);
        }

        // refresh instance
        $this->getById($this->id);

        // update log
        $this->gLog->updateUser($this->id);

        return true;
    }


    /***************************************************************************
    setRoles
    Updates the user roles.
    ***/
    public function setRoles($roles) {

        if(!is_array($roles)) {
            return $this;
        }

        // delete then insert roles of user
        $link = $this->dbLink->create()->open()
            ->bind(':user', $this->id)
            ->prep("DELETE FROM t_userUserRole WHERE userUserRole_user = :user")
            ->exec()
            ->prep("
                INSERT INTO t_userUserRole
                    (userUserRole_user, userUserRole_role)
                VALUES
                    (:user, :role)");

        foreach($roles as $role) {
            $link->bind(':role', $role)->exec();
            if(!$link->success()) {
                exit($link->error());
            }
        }

        // update log
        $this->gLog->updateUserUserRole($this->id);

        return $this;
    }


    /***************************************************************************
    getError
    Gets error code
    ***/
    public function getError() {
        return $this->errorCode;
    }
}
?>
