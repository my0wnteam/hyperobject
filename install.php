<?php include 'init.php'; ?>
<?php
if (isset($_GET['fix'])) {
	$fix = clearFormInput($_GET['fix']);
	$tname = NULL;
	$cname = NULL;
	if (isset($_GET['tname'])) {
		$tname = clearFormInput($_GET['tname']);
	}
	if (isset($_GET['cname'])) {
		$cname = clearFormInput($_GET['cname']);
	}
	
	switch ($fix) {
		case 'table':
			$hyperclass = new $tname();
			$hyperclass->install();
			break;
		case 'column':
			$hyperclass = new $tname();
			$hyperclass->install_col($cname);
			break;
	}
}
?>
<?php Html_skeleton::get_head(); ?>
<?php Html_skeleton::get_header(); ?>
	<div id="main">
		<?php echo Html_skeleton::get_sidebar(); ?>
		<div id="main_content">
			<h1 class="text_center">Installation</h1>
<?php
foreach ($GLOBALS['project']->array_entities as $entity) {
	//Dynamic version
	$hyperclass = new $entity();
	
	//echo $hyperclass->database_table->getString();
	
	//Checking Attr database
	$results = $hyperclass->install_checkDbCols();
	
	foreach ($results->tables as $table => $result) {
		echo '<table>';
		echo '<caption><h2>'.$hyperclass->name.'</h2></caption>';
		echo '<thead>';
		echo '<tr>';
		echo '<th>Name</th>';
		echo '<th>Type</th>';
		echo '<th>Status</th>';
		echo '<th>Action</th>';
		echo '</tr>';
		echo '</thead>';
		
		//Table
		echo '<tbody>';
		echo '<tr>';
		echo '<td>'.$table.'</td>';
		echo '<td>Table</td>';
		echo '<td>'.Database_check::errorMessage($result["code"]).'</td>';
		echo '<td>';
		if ($result["code"]) {
			//Error detected - take action
			echo '<a href="?fix=table&tname='.$table.'">Fix it</a>';
		}
		echo '</td>';
		echo '</tr>';
		
		//Columns
		foreach ($result["columns"] as $column => $code) {
			echo '<tr>';
			echo '<td>'.$column.'</td>';
			echo '<td>Column</td>';
			echo '<td>'.Database_check::errorMessage($code).'</td>';
			echo '<td>';
			if ($code) {
				//Error detected - take action
				echo '<a href="?fix=column&tname='.$table.'&cname='.$column.'">Fix it</a>';
			}
			echo '</td>';
			echo '</tr>';
		}
		
		echo '</tbody>';
		echo '</table>';
	}
	
	
}
?>
		</div>
	</div>
<?php Html_skeleton::get_foot(); ?>