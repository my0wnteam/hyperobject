<?php

/* COLUMN */

class Html_table_column {
	public $column_label;
	public $column_option_search;
	public $column_option_filter;
	public $column_option_sort;
	public $column_actions;
	public $attr;
	
	public static function createWithParams($label, $column_option_search=FALSE, $column_option_filter=FALSE, $column_option_sort=FALSE) {
		$table_column = new self();
		
		$table_column->column_label = $label;
		$table_column->column_option_search = $column_option_search;
		$table_column->column_option_filter = $column_option_filter;
		$table_column->column_option_sort = $column_option_sort;
		$table_column->column_actions = array();
		
		return $table_column;
	}
	
	public static function createFromAttr($attr, $class, $column_option_search, $column_option_filter, $column_option_sort) {
		$table_column = Html_table_column::createWithParams("", $column_option_search, $column_option_filter, $column_option_sort);
		
		$table_column->attr = &$attr;
		$table_column->column_label = $table_column->attr->getClassName($class);
		
		return $table_column;
	}
	
	function generateHtml($data, $primary_key_value=NULL) {
		$data_wrap = FALSE;
		
		$html = '';
		
		foreach ($this->column_actions as $column_action) {
			if ($column_action->action_wrap && !$data_wrap) {
				//Only data_wrap once
				$data_wrap = TRUE;
				$html .= $column_action->generateHtml($primary_key_value, $data);
			} else {
				$html .= $column_action->generateHtml($primary_key_value);
			}
			$html .= " "; //space between element
		}
		
		if (!$data_wrap) {
			//No data wrap, add element
			$html = $data.trim($html);
		}
		
		return '<td>'.$html.'</td>';
	}
	
	function generateHtmlTitle() {
		if ($this->column_option_sort) {
			if (isset($_GET['sort'])) {
				if ($_GET['sort'] === $this->attr->name) {
					if (isset($_GET['sort_order'])) {
						if ($_GET['sort_order']==="asc") {
							//Offer desc
							return '<th><a href="'.UrlQueryRebuild(array("sort" => $this->attr->name, "sort_order" => "desc")).'">'.$this->column_label.' ▲</a></th>';
						} else {
							//Offer asc
							return '<th><a href="'.UrlQueryRebuild(array("sort" => $this->attr->name, "sort_order" => "asc")).'">'.$this->column_label.' ▼</a></th>';
						}
					} else {
						return '<th><a href="'.UrlQueryRebuild(array("sort" => $this->attr->name, "sort_order" => "asc")).'">'.$this->column_label.' ▲</a></th>';
					}
				} else {
					return '<th><a href="'.UrlQueryRebuild(array("sort" => $this->attr->name, "sort_order" => "asc")).'">'.$this->column_label.'</a></th>';
				}
			} else {
				return '<th><a href="'.UrlQueryRebuild(array("sort" => $this->attr->name, "sort_order" => "asc")).'">'.$this->column_label.'</a></th>';
			}
		} else {
			return '<th>'.$this->column_label.'</th>';
		}
	}
	
	function addAction($action) {
		$this->column_actions[] = &$action;
	}
}

/* ACTION */

class Html_table_action {
	public $action_url;
	public $action_label;
	public $action_columns;
	public $action_wrap;
	
	public static function createWithParams($url, $label, $columns, $action_wrap) {
		$table_action = new self();
		
		$table_action->action_url = $url;
		$table_action->action_label = $label;
		$table_action->action_columns = $columns;
		$table_action->action_wrap = $action_wrap;
		
		return $table_action;
	}
	
	function generateHtml($primary_key_value, $wrap_around="") {
		if (!empty($wrap_around)) {
			return '<a href="'.$this->action_url.$primary_key_value.'">'.$wrap_around.'</a>';
		} else {
			return '<a class="button small_button button_gray" href="'.$this->action_url.$primary_key_value.'">'.$this->action_label.'</a>';
		}
	}
}

/* TABLE */

class Html_table {
	public $table_mode_js;
	public $table_option_search;
	public $table_option_filter;
	public $table_option_sort;
	public $table_option_export;
	public $table_option_pages;
	public $table_option_perpage;
	public $table_option_checkbox;
	public $table_option_header_title;
	public $table_option_footer_title;

	public $header;
	public $css_classes;
	public $footer;	

	public $columns;
	public $actions;
	
	private $hyperclass_ptr;

    public function __construct() {
        //$this->data = NULL;
		$this->css_classes = "table_style";
		$this->header = '<table class="table_style">';
		$this->footer = '</table>';
		$this->hyperclass_ptr = NULL;
		
		$this->columns = array();
		$this->actions = array();
    }
	
	public static function createWithPtr($hyperclass) {
		$html_table = new self();
		
		$html_table->setHyperClass($hyperclass);
		
		//Default actions: edit, delete
		$html_table->addAction(generateUrl(array("action"=>"form", "class"=>$html_table->hyperclass_ptr->name))."&data_id=", $GLOBALS['project']->language->get('edit'), array("actions", $html_table->hyperclass_ptr->getPrimaryKey()->name), TRUE);
		$html_table->addAction("delete.php?source_page=".urlencode($_SERVER['REQUEST_URI'])."&data_class=".$html_table->hyperclass_ptr->name."&data_id=", $GLOBALS['project']->language->get('delete'), array("actions"));
		
		return $html_table;
	}
	
	public function setHyperClass($hyperclass) {
		$this->hyperclass_ptr = &$hyperclass;
	}
	
	private function generateHtmlHeader() {
		$this->header = '<table class="'.$this->css_classes.'">';
	}
	
	public function addColumn($attr, $option_search=TRUE, $option_filter=TRUE, $option_sort=TRUE) {
		$this->columns[$attr->name] = Html_table_column::createFromAttr($attr, $this->hyperclass_ptr->name, $option_search, $option_filter, $option_sort);
	}
	
	public function clearColumns() {
		foreach ($this->columns as $column) {
			unset($column);
		}
	}
	
	public function addAction($url, $label, $columns, $action_wrap=FALSE) {
		$this->actions[] = Html_table_action::createWithParams($url, $label, $columns, $action_wrap);
	}
	
	public function clearActions() {
		foreach ($this->actions as $action) {
			unset($action);
		}
	}
	
	public function generateHtml() {
		$html = "";
		//Primary key
		$primary_key = $this->hyperclass_ptr->getPrimaryKey()->name;
		
		//Link actions to columns
		foreach ($this->actions as $action) {
			if (empty($action->action_columns)) {
				//No action column, add to extra column
				if (!isset($this->columns["actions"])) {
					$this->columns["actions"] = Html_table_column::createWithParams($GLOBALS['project']->language->get("actions"));
				}
				
				//Add action to action column
				$this->columns["actions"]->addAction($action);
			} else {
				foreach ($action->action_columns as $action_column) {
					if (!isset($this->columns[$action_column])) {
						//Create column first
						$this->columns[$action_column] = Html_table_column::createWithParams($GLOBALS['project']->language->get($action_column));
					}
					$this->columns[$action_column]->addAction($action);
				}
			}
		}
		
		/* BEFORE TABLE */
		//Array filters
		foreach ($this->columns as $column) {
			if ($column->column_option_filter) {
				//Can filter
				$html_filter = "";
				
				$array_column_data = array_unique(extractArrayColumn($this->hyperclass_ptr->data, $column->attr->name));
				
				$html_filter .= '<form class="inline_block" action="'.$_SERVER['REQUEST_URI'].'" method="POST"><select name="'.$column->attr->name.'"><option value="">'.$GLOBALS['project']->language->get('any').' '.$column->column_label.'</option>';
				foreach ($array_column_data as $data) {
					$html_filter .= '<option value="'.$data.'"';
					if ((isset($_GET['filter'])) && (isset($_GET['filter_value']))) {
						if (($_GET['filter'] === $column->attr->name) && ($_GET['filter_value']==$data)) {
							$html_filter .= ' selected';
						}
					}
					//$html_filter .= '>'.$data.'</option>';
					//Change value
					$this->hyperclass_ptr->attr[$column->attr->name]->set($data);
					$html_filter .= '>'.$this->hyperclass_ptr->attr[$column->attr->name]->displayValue().'</option>';
				}
				$html_filter .= '<input type="submit" name="filter" value="'.$GLOBALS['project']->language->get('filter').'" /><input type="hidden" name="filter_attr" value="'.$column->attr->name.'" /></form>';
				
				$html .= $html_filter;
			}
		}
		
		/* BEGIN TABLE */
		$this->generateHtmlHeader();
		/* HEADER */
		$html .= $this->header;
		
		//Columns names
		$html .= '<thead>';
		$html .= '<tr>';
		
		foreach ($this->columns as $column) {
			$html .= $column->generateHtmlTitle();
		}
		
		$html .= '</tr>';
		$html .= '</thead>';
		
		/* BODY */
		
		//Columns data
		if (count($this->hyperclass_ptr->data)) {
			$html .= '<tbody>';
			
			//Loop on number of lines
			/*foreach ($this->hyperclass_ptr->data as $data) {
				$html .= '<tr>';
			
				//Loop on columns
				foreach ($this->columns as $column) {
					$html .= $column->generateHtml($data[$column->attr->name]);
				}
			
				$html .= '</tr>';
			}*/
			
			//Same function but with pointer instead of data
			$this->hyperclass_ptr->ptrBegin();
			do {
				//Get the primary key value
				$primary_key_value = $this->hyperclass_ptr->attr[$primary_key]->get();
				
				$html .= '<tr>';
			
				//Loop on columns
				foreach ($this->columns as $column) {
					//Test if column is in hyperobject
					if (isset($column->attr->name)) {
						$html .= $column->generateHtml($this->hyperclass_ptr->attr[$column->attr->name]->displayValue(), $primary_key_value);
					} else {
						//action column
						$html .= $column->generateHtml("", $primary_key_value);
					}
				}
			
				$html .= '</tr>';
			} while ($this->hyperclass_ptr->ptrNext());
			
			$html .= '</tbody>';
		}
		
		/* FOOTER */
		
		//Columns names
		$html .= '<tfoot>';
		$html .= '<tr>';
		
		foreach ($this->columns as $column) {
			$html .= $column->generateHtmlTitle();
		}
		
		$html .= '</tr>';
		$html .= '</tfoot>';
		
		$html .= $this->footer;
		
		return $html;
	}
}