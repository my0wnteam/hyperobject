<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperobject_attr.php";

abstract class HyperObjectData extends HyperObjectAttr {
	public $data; //Contain data loaded from SQL
	public $data_ptr; //Data pointer
	
    public function __construct() {
		parent::__construct();
		$this->data = array();
		$this->data_ptr = null;
    }

/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
	}

	/* POINTER FUNCTIONS */
	
	//Pointer
	public function ptrBegin() {
		$this->setPtr(0);
	}
	
	public function ptrEnd() {
		$this->setPtr(count($this->data)-1);
	}
	
	public function ptrNext() {
		$this->data_ptr++;
		if ($this->data_ptr >= count($this->data)) {
			return NULL;
		}
		$this->dataToAttr();
		return $this->data_ptr;
	}
	
	public function ptrPrevious() {
		$this->data_ptr--;
		if ($this->data_ptr < 0) {
			return NULL;
		}
		$this->dataToAttr();
		return $this->data_ptr;
	}
	
	public function setPtr($ptr = null) {
		if ($ptr !== null) {
			$this->data_ptr = $ptr;
			$this->dataToAttr();
		}
	}
	
	public function setPtrId($id = 0) {
		//Look for Id in data and place pointer
		$ptr = $this->getPtrFromId($id);
		
		if ($ptr !== false) {
			$this->setPtr($ptr);
			return true;
		} else {
			return false;
		}
	}
	
	public function getPtrFromId($id = 0) {
		//Look for Id in data
		return array_search($id, array_column($this->data, $this->getPrimaryKey()->name));
	}
	
	/* SETTER AND GETTER */

	//Setter and Getter
	public function get($attr = "", $ptr = null) {
		/* TODO : fix old code */
		
		//Place pointer
		$this->setPtr($ptr);
		
		if (isset($this->data[$this->data_ptr][$attr])) {
			return $this->data[$this->data_ptr][$attr];
		} else {
			return NULL;
		}
	}
	
	public function set($attr = "", $val = "", $ptr = null) {
		//Place pointer
		$this->setPtr($ptr);
		
		//Data record exists
		if (isset($this->data[$this->data_ptr])) {
			//Use Attr as controller
			$this->data[$this->data_ptr][$attr] = $val;
			return true;
		} else {
			return false;
		}
	}
	
	/* CHECK ERROR */
	public function errorCheck() {
		foreach ($this->attr as $attr) {
			if ($attr->error !== FALSE) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/* DATA <=> ATTR */
	
	public function dataToAttr() {
		//Load from data to attr
		foreach ($this->attr as $attr) {
			if (isset($this->data[$this->data_ptr][$attr->name])) {
				$this->attr[$attr->name]->set($this->data[$this->data_ptr][$attr->name]);
			} else {
				$this->attr[$attr->name]->set($this->attr[$attr->name]->default_value);
			}
		}
	}
	
	public function attrToData() {
		//Save from attr to existing data
		foreach ($this->attr as $attr) {
			$this->data[$this->data_ptr][$attr->name] = $attr->get();
		}
	}
	
	public function attrNewData() {
		//Save from attr to new data
		$data = array();
		foreach ($this->attr as $attr) {
			$data[$attr->name] = $attr->get();
		}
		$this->data[] = $data;
		
		//Place pointer to data
		$this->data_ptr = count($this->data)-1;
	}
}