<?php

if (isset($_POST)) {
	if (isset($_POST['filter'])) {
		$attr = $_POST['filter_attr'];
		$value = $_POST[$attr];
		
		//POST -> GET
		header("Location: ".UrlQueryRebuild(array("filter" => $attr, "filter_value" => $value)));
		die;
	}
}

$db_class = NULL;
$table_name = "";

if (isset($_GET)) {
	if (isset($_GET['class'])) {
		$data_class = clearFormInput($_GET['class']);
			
		//create class
		$db_class = new $data_class();
		//Load data
		if (isset($_GET['filter']) && isset($_GET['filter_value'])) {
			//filter
			$filter = clearFormInput($_GET['filter']);
			$filter_value = clearFormInput($_GET['filter_value']);
			
			if (!empty($filter_value)) {
				$db_class->loadQuery("SELECT * FROM ".$db_class->name." WHERE ".$filter."='".$filter_value."' ORDER BY ".$db_class->getPrimaryKey()->name);
			} else {
				$db_class->loadAll();
			}
		} else {
			$db_class->loadAll();
		}
	}
	if (isset($_GET['table_name'])) {
		$table_name = clearFormInput($_GET['table_name']);
	}
	if (isset($_GET['sort'])) {
		//Sort data
		$GLOBALS['sort_attr'] = clearFormInput($_GET['sort']);
		
		if (isset($_GET['sort_order'])) {
			if ($_GET['sort_order']==="desc") {
				usort($db_class->data, function($a,$b){return strcmp($b[$GLOBALS['sort_attr']], $a[$GLOBALS['sort_attr']]);});
			} else {
				usort($db_class->data, function($a,$b){return strcmp($a[$GLOBALS['sort_attr']], $b[$GLOBALS['sort_attr']]);});
			}
		} else {
			usort($db_class->data, function($a,$b){return strcmp($a[$GLOBALS['sort_attr']], $b[$GLOBALS['sort_attr']]);});
		}
	}
}

//RENDER
?>
<h1 class="text_center"><?php echo $GLOBALS['project']->language->get('table'); ?> <?php if ($db_class) { echo $db_class->name; } ?></h1>
<?php
if ($db_class) {
	//Display data table
	echo $db_class->getHtmlButton();
	echo $db_class->getHtmlTable($table_name);
}