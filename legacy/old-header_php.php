<?php
$valid=0;
$errmsg="";
$acc_email="";


/* CHECK LOGIN */
if (!(isset($_SESSION['ID']))) {
	$acc_email = checkRemember();
	if (!empty($acc_email)) {
		$valid=rememberMeSession($acc_email);
	}
} else {
	$valid=1;
}

/** LOG **/
if($valid) {
	logRequest($_SESSION['ID']);
}

$login_email = "";
$login_password = "";

if ((isset($_POST['login_submit'])) && (!$valid)) {
	$whitelist = array('login_email','login_password','login_submit');
	foreach ($_POST as $key=>$item) {
		if (!in_array($key, $whitelist)) {
			die("Please use only the fields in the form");
		}
	}
	$login_email = clearFormInput($_POST['login_email']);
	$login_password = clearFormInput($_POST['login_password']);
	$valid=1;
	
	$validEmail = validEmail($login_email);
	if (empty($login_email)) {
		$valid=0;
		$errmsg = "e-mail manquant";
	} else if (empty($login_password)) {
		$valid=0;
		$errmsg = "Mot de passe manquant";
	} else if (!(validEmail($login_email))) {
		$valid=0;
		$errmsg = "e-mail invalide";
	} else if (!(checkEmailPwd($login_email,$login_password))) {
		$valid=0;
		$errmsg = "Mot de passe invalide";
	}
	
	if ($valid) {
		rememberMeCookie($login_email);
		$valid = rememberMeSession($login_email);
		updateLastVisit($_SESSION['ID']);
		
		header("Location: index.php");
		die;
	}
}

//Login, go to index
if ((isset($_POST['login_submit'])) && (isset($_SESSION['ID']))) {
	if (strpos($_SERVER['PHP_SELF'],"index.php") == FALSE) {
		header("Location: index.php");
		die;
	}
}

/* DISCONNECT */
if (isset($_GET['disconnect'])) {
	if (isset($_SESSION['ID'])) {

		$reqsql_disconnect = "SELECT acc_remme FROM t_account WHERE id_acc=".$_SESSION['ID'];
		$ressql_disconnect = $wrapper_db->exe_query($reqsql_disconnect) or die(mysqli_error($link));
		$row_disconnect = mysqli_fetch_array($ressql_disconnect);
		$sessions = unserialize($row_disconnect['acc_remme']);

		$index_to_delete = -1;

		if ($sessions) {
			foreach ($sessions as $index=>$session) {
				if ($session['TOKEN'] == $_COOKIE['remmecookie_mobycms']) {
					if ($session['IP'] == $_SERVER['REMOTE_ADDR']) {
						$index_to_delete = $index;
						break;
					}
				}
			}
		}

		if ($index_to_delete>=0) {
			unset($sessions[$index_to_delete]);
			$sessions = serialize($sessions);
			$reqsql_disconnect = "UPDATE t_account SET acc_remme='".$sessions."' WHERE id_acc=".$_SESSION['ID'];
			$wrapper_db->exe_query($reqsql_disconnect) or die(mysqli_error($link));
		}
	}
	session_unset();
	if (isset($_COOKIE[session_name()])) {
		setcookie("remmecookie_mobycms","",time()-60*60);
	}
	session_destroy();
}

?>