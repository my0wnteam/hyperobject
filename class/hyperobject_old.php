<?php

define("RIGHT_READ", 1);
define("RIGHT_WRITE", 2);
define("HIDDEN_TABLE", 4);
define("HIDDEN_FORM", 8);

require_once "project.php";
require_once "database_check.php";

/**********
***********
**********/
/* HYPER ATTRIBUTE */
/**********
***********
**********/

abstract class HyperAttribute {
	public $name;
	public $value;
	public $error;
	public $type;
	public $flags; //RIGHT_READ|RIGHT_WRITE|HIDDEN_TABLE|HIDDEN_FORM
	public $database_column;
	
	private $form_style;
	
	public function set($val) {
		$this->value = $val;
		if (!$this->isRightType($this->value)) {
			$this->error = "Wrong type: ".gettype($this->value)." Value: ".$this->value;
		} else {
			$this->error = FALSE;
		}
	}
	
	public function get() {
		return $this->value;
	}
	
	public function displayValue() {
		return $this->value;
	}
	
	abstract function isRightType($val);
	
	abstract function getHtml(); //form + label + style
	
	public function getLabel() {
		return '<label for="'.$this->name.'">'.$this->name.'</label>';
	}
	
	public function getClassLabel($class) {
		return '<label for="'.$this->name.'">'.$GLOBALS['project']->language->getClass($class, $this->name).'</label>';
	}
	
	//abstract function getPhp();
	
	public function getSql() {
		$this->database_column->getString();
	}
	
	public function &getSqlColumn() {
		return $this->database_column;
	}
	
	public function install_checkDbCol($table) {
		$query = "SHOW COLUMNS FROM ".$table." LIKE '".$this->name."'";
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		return $result->num_rows;
	}
}

class HyperAttribute_ID extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "id";
		$this->flags = RIGHT_READ;
		$this->database_column = DatabaseColumn::createIdFromString($this->name.' INT (11) UNSIGNED NOT NULL AUTO_INCREMENT');
		
		$this->form_style = "STYLE_NONE_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="hidden" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_String extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "string";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (255) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_string($val);
	}
	
	public function getHtml() {
		$html = '<input type="text" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Text extends HyperAttribute_String {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "text";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' TEXT NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function getHtml() {
		$html = '<textarea id="'.$this->name.'" name="'.$this->name.'" placeholder="">'.$this->get().'</textarea>';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Email extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = "";
		$this->error = FALSE;
		$this->type = "email";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' VARCHAR (255) NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		//return is_string($val);
		return is_email($val);
	}
	
	public function getHtml() {
		$html = '<input type="email" id="'.$this->name.'" name="'.$this->name.'" placeholder="" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Int extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "int";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (11) NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="number" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Datetime extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetime";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL');
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return TRUE; //is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<input type="datetime-local" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->get().'" />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_DatetimeStampCreate extends HyperAttribute_Datetime {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetimestamp_create";
		$this->flags = RIGHT_READ|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
		
		$this->form_style = "STYLE_INPUT";
	}
}

class HyperAttribute_DatetimeStampModify extends HyperAttribute_Datetime {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "datetimestamp_modify";
		$this->flags = RIGHT_READ|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
		//Extra trigger
		$this->database_column->extra = "ON UPDATE CURRENT_TIMESTAMP";
		
		$this->form_style = "STYLE_INPUT";
	}
}

class HyperAttribute_Bool extends HyperAttribute {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "bool";
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (1) UNSIGNED NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_SPACE_INPUT_LABEL";
	}
	
	//toBool
	public function set($val) {
		$this->value = toBool($val);
		if (!$this->isRightType($this->value)) {
			$this->error = "Wrong type: ".gettype($this->value)." Value: ".$this->value;
		} else {
			$this->error = FALSE;
		}
	}
	
	public function isRightType($val) {
		return is_bool($val);
	}
	
	public function getHtml() {
		$html = '<input type="checkbox" id="'.$this->name.'" name="'.$this->name.'"';
		if ($this->get()) {
			$html .= ' checked';
		}
		$html .= ' />';
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
}

class HyperAttribute_Delete extends HyperAttribute_Bool {
	public function __construct($name) {
		$this->name = $name;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "booldelete";
		$this->flags = RIGHT_READ|HIDDEN_TABLE|HIDDEN_FORM;
		$this->database_column = DatabaseColumn::createFromString($this->name.' INT (1) UNSIGNED NOT NULL DEFAULT 0');
		
		$this->form_style = "STYLE_SPACE_INPUT_LABEL";
	}
}

/* FOREIGN KEY */

class HyperAttribute_ForeignKey extends HyperAttribute {
	private $foreign_table;
	private $foreign_key;
	private $foreign_display_field;
	
	public function __construct($name, $foreign_table, $foreign_key, $foreign_display_field="") {
		$this->name = $name;
		$this->foreign_table = $foreign_table;
		$this->foreign_key = $foreign_key;
		$this->foreign_display_field = $foreign_display_field;
		$this->value = 0;
		$this->error = FALSE;
		$this->type = "id";
		$this->default_value = NULL;
		$this->flags = RIGHT_READ|RIGHT_WRITE;
		$this->database_column = DatabaseColumn::createFkFromString($this->name.' INT (11) UNSIGNED NOT NULL', $this->foreign_table, $this->foreign_key);
		
		$this->form_style = "STYLE_LABEL_INPUT";
	}
	
	public function isRightType($val) {
		return is_numeric($val);
	}
	
	public function getHtml() {
		$html = '<select id="'.$this->name.'" name="'.$this->name.'">';
		
		//Create foreign object
		$foreign_object = new $this->foreign_table();
		$foreign_object->loadAll();
		
		foreach ($foreign_object->data as $data_row) {
			$html .= '<option value="'.$data_row[$this->foreign_key].'"';
			if ($this->get()===$data_row[$this->foreign_key]) {
				$html .= ' selected';
			}
			$html .= '>';
			if (!empty($this->foreign_display_field)) {
				$html .= $data_row[$this->foreign_display_field];
			} else {
				$html .= $data_row[$this->foreign_key];
			}
			$html .= '</option>';
		}
		
		$html .= '</select>';
		
		if ($this->error) {
			$html .= $this->error;
		}
		return $html;
	}
	
	public function displayValue() {
		if (!empty($this->foreign_display_field)) {
			$foreign_object = new $this->foreign_table();
			$foreign_object->load($this->value);
			return $foreign_object->attr[$this->foreign_display_field]->get();
		} else {
			return $this->value;
		}
	}
}

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

abstract class HyperObject {
    public $name; //Class name
	public $attr; //Array of HyperAttribute
	public $database_table; //Object based on attr, with SQL database structure
	public $data; //Contain data loaded from SQL
	public $data_ptr; //Data pointer
	
	//Structures generated
	public $html_tables;
	public $html_forms;
	
    public function __construct() {
		//$this->constructor();
		$this->name = get_class($this);
		$this->attr = array();
		
        $this->database_table = new DatabaseTable();
		$this->database_table->name = $this->name;
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
    }

/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
        $this->database_table = new DatabaseTable();
		$this->database_table->name = $this->name;
		//Add ID
		//Add Primary Key constraint
		//$this->database_table->columns[] = DatabaseColumn::createIdFromString('id INT (11) UNSIGNED NOT NULL AUTO_INCREMENT');
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
	}
	
/** Initialize, load attributes
**/
	public function init() {
		$this->database_table->loadAttrs($this->attr);
	}
	
	/* INSTALL */
	
/** Create table
* @return {string} Query result
**/
	public function install() {
		return $GLOBALS['project']->database_connexion->db_query($this->database_table->getString());
		//return $GLOBALS['project']->database_connexion->db_query($this->database_table->getString());
	}

/** Drop table
* @return {string} Query result
**/
	public static function uninstall() {
		$table = new DatabaseTable();
		$table->loadString(static::$query);
		
		$query = "DROP TABLE ".$table->name;
		return $GLOBALS['project']->database_connexion->db_query($query);
	}
	
	public function install_checkDbTable() {
		$query = "SHOW TABLES LIKE '".$this->name."'";
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		return $result->num_rows;
	}
	
	public function install_checkDbCols() {
		//$result = array();
		$result = new Database_check();
		
		if (!$this->install_checkDbTable()) {
			//$result[] = "Table ".$this->name." doesnt exist";
			$result->addTable($this->name, DBCHECK_ERROR_MISSING);
		} else {
			$result->addTable($this->name, DBCHECK_OK);
			foreach ($this->attr as $attr) {
				if (!$attr->install_checkDbCol($this->name)) {
					//$result[] = "Column ".$attr->name." doesnt exist";
					$result->addColumn($attr->name, $this->name, DBCHECK_ERROR_MISSING);
				} else {
					$result->addColumn($attr->name, $this->name, DBCHECK_OK);
				}
			}
		}
		return $result;
	}
	
	public function install_col($col) {
		return $GLOBALS['project']->database_connexion->db_query($this->database_table->getColString($col));
	}
	
	/* ATTRIBUTES */
	
/** Add attribute to object array
**/
	public function addAttr($attr) {
		$this->attr[$attr->name] = $attr;
	}
	
	public function searchAttr($type, $multiple_result = FALSE) {
		$result = FALSE;
		if ($multiple_result) {
			$result = array();
		}
		
		foreach ($this->attr as $attr) {
			if ($attr->type === $type) {
				if ($multiple_result) {
					$result[] = $attr;
				} else {
					$result = $attr;
					break;
				}
			}
		}
		
		return $result;
	}
	
	public function getPrimaryKey() {
		foreach ($this->attr as $attr) {
			if ($attr->type === "id") {
				return $attr;
			}
		}
		return NULL;
	}
	
	/* POINTER FUNCTIONS */
	
	//Pointer
	public function ptrBegin() {
		$this->setPtr(0);
	}
	
	public function ptrEnd() {
		$this->setPtr(count($this->data)-1);
	}
	
	public function ptrNext() {
		$this->data_ptr++;
		if ($this->data_ptr >= count($this->data)) {
			return NULL;
		}
		$this->dataToAttr();
		return $this->data_ptr;
	}
	
	public function ptrPrevious() {
		$this->data_ptr--;
		if ($this->data_ptr < 0) {
			return NULL;
		}
		$this->dataToAttr();
		return $this->data_ptr;
	}
	
	public function setPtr($ptr = null) {
		if ($ptr !== null) {
			$this->data_ptr = $ptr;
			$this->dataToAttr();
		}
	}
	
	public function setPtrId($id = 0) {
		//Look for Id in data and place pointer
		$ptr = $this->getPtrFromId($id);
		
		if ($ptr !== false) {
			$this->setPtr($ptr);
			return true;
		} else {
			return false;
		}
	}
	
	public function getPtrFromId($id = 0) {
		//Look for Id in data
		return array_search($id, array_column($this->data, $this->getPrimaryKey()->name));
	}
	
	public function dataToAttr() {
		//Load from data to attr
		foreach ($this->attr as $attr) {
			if (isset($this->data[$this->data_ptr][$attr->name])) {
				//$this->attr[$attr->name]->value = $this->data[$this->data_ptr][$attr->name];
				$this->attr[$attr->name]->set($this->data[$this->data_ptr][$attr->name]);
			} else {
				//$this->attr[$attr->name]->value = $this->attr[$attr->name]->database_column->defaultvalue;
				$this->attr[$attr->name]->set($this->attr[$attr->name]->getSqlColumn()->defaultvalue);
			}
		}
	}
	
	public function attrToData() {
		//Save from attr to existing data
		foreach ($this->attr as $attr) {
			$this->data[$this->data_ptr][$attr->name] = $attr->get();
		}
	}
	
	public function attrNewData() {
		//Save from attr to new data
		$data = array();
		foreach ($this->attr as $attr) {
			$data[$attr->name] = $attr->get();
		}
		$this->data[] = $data;
		
		//Place pointer to data
		$this->data_ptr = count($this->data)-1;
	}
	
	/* SETTER AND GETTER */
	//Work with attr value
	
	//Put in attr
	public function catchPost() {
		//Submit ok
		//Put from POST to attr
		if (isset($_POST['submit_'.$this->name])) {
			foreach ($this->attr as $attr) {
				//Set data in attr
				if (isset($_POST[$attr->name])) {
					//$this->attr[$attr->name]->value = $_POST[$attr->name];
					$this->attr[$attr->name]->set($_POST[$attr->name]);
				}
			}
			
			//New or update ?
			if (!empty($this->attr[$this->getPrimaryKey()->name]->get())) {
				//Place pointer
				if ($this->setPtrId($this->attr[$this->getPrimaryKey()->name]->get())) {
					$this->attrToData();
				} else {
					$this->attrNewData();
				}
			} else {
				$this->attrNewData();
			}
		}
	}
	
	//Used in post.php
	public function errorCheck() {
		foreach ($this->attr as $attr) {
			if ($attr->error !== FALSE) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	//Setter and Getter
	public function get($attr = "", $ptr = null) {
		/* TODO : fix old code */
		
		//Place pointer
		$this->setPtr($ptr);
		
		if (isset($this->data[$this->data_ptr][$attr])) {
			return $this->data[$this->data_ptr][$attr];
		} else {
			return NULL;
		}
		
		/*$i_column = $this->database_table->findColumnByName($attr);
		if ($i_column !== false) {
			switch ($this->database_table->columns[$i_column]->type) {
				default:
					return $this->database_table->columns[$i_column]->type;
			}
		}*/
	}
	
	public function set($attr = "", $val = "", $ptr = null) {
		//Place pointer
		$this->setPtr($ptr);
		
		//Data record exists
		if (isset($this->data[$this->data_ptr])) {
			//Use Attr as controller
			$this->data[$this->data_ptr][$attr] = $val;
			return true;
		} else {
			return false;
		}
	}
	
	/* LOAD AND SAVE */
	//Work with data array
	
	//Get multiple objects
	public function loadAll() {
		$query = "SELECT * FROM ".$this->name;
		if ($booldelete = $this->searchAttr("booldelete")) {
			$query .= " WHERE ".$booldelete->name."=0";
		}
		$query .= " ORDER BY ".$this->getPrimaryKey()->name;
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		if ($result === false) {
			echo $GLOBALS['project']->database_connexion->db_error();
		} else {
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$this->data[] = $row;
			}
		}
	}
	
	//Get query objects
	public function loadQuery($query = "") {
		//TODO
	}
	
	//Get single object
	public function load($id) {
		$query = "SELECT * FROM ".$this->name." WHERE ".$this->getPrimaryKey()->name."=?";
		if ($booldelete = $this->searchAttr("booldelete")) {
			$query .= " AND ".$booldelete->name."=0";
		}
		$result = $GLOBALS['project']->database_connexion->db_query($query, array($id));
		
		if ($result === false) {
			echo $GLOBALS['project']->database_connexion->db_error();
		} else {
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$this->data[] = $row;
			}
			
			//place pointer on last data inserted
			$this->setPtr(count($this->data)-1);
		}
	}
	
	//Save to database
	public function save($ptr = null) {
		//Place pointer
		$this->setPtr($ptr);
		
		$query = "";
		$query_cols = "";
		$query_vals = "";
		$query_key_val = "";
		
		foreach ($this->attr as $attr) {
			//if ($attr->type != "id") {
			if ($attr->flags & RIGHT_WRITE) {
				if (isset($this->data[$this->data_ptr][$attr->name])) {
					$query_cols .= $attr->name.",";
					$query_vals .= "'".$this->data[$this->data_ptr][$attr->name]."',";
					
					$query_key_val .= $attr->name."='".$this->data[$this->data_ptr][$attr->name]."',";
				}
			}
		}
		//Remove last ,
		$query_cols = substr($query_cols, 0, -1);
		$query_vals = substr($query_vals, 0, -1);
		$query_key_val = substr($query_key_val, 0, -1);
		
		if ($this->data[$this->data_ptr][$this->getPrimaryKey()->name]) {
			//Primary key exists = update
			$query = "UPDATE ".$this->name." SET ";
			
			//key = value
			$query .= $query_key_val;
			
			//where
			$query .= " WHERE ".$this->getPrimaryKey()->name."=".$this->data[$this->data_ptr][$this->getPrimaryKey()->name];
		} else {
			//create
			$query = "INSERT INTO ".$this->name;
			//columns
			$query .= " (";
			$query .= $query_cols;
			$query .= ")";
			//values
			$query .= " VALUES (";
			$query .= $query_vals;
			$query .= ")";
		}
		
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		if ($result === false) {
			return $GLOBALS['project']->database_connexion->db_error();
		} else {
			return $result;
		}
	}
	
	public function saveAll() {
		for ($i=0; $i<count($this->data); $i++) {
			save($i);
		}
	}
	
	/* LOAD SESSION POSTDATA */
	
	public function loadSession() {
		//Reload POST data into attr
		foreach ($this->attr as $attr) {
			if (isset($_SESSION['post_data'][$attr->name])) {
				//$attr->value = $_SESSION['post_data'][$attr->name];
				$attr->set($_SESSION['post_data'][$attr->name]);
			}
		}
	}
	
	/* DELETE DATA */
	
	public function deleteData($id) {
		if ($booldelete = $this->searchAttr("booldelete")) {
			//Delete attr
			$query = "UPDATE ".$this->name." SET ".$booldelete->name."=1 WHERE ".$this->getPrimaryKey()->name."=".$id;
		} else {
			//No delete attr
			$query = "DELETE FROM ".$this->name." WHERE ".$this->getPrimaryKey()->name."=".$id;
		}
		
		$result = $GLOBALS['project']->database_connexion->db_query($query);
			
		if ($result === false) {
			return $GLOBALS['project']->database_connexion->db_error();
		} else {
			return $result;
		}
	}
	
	/* HTML FORM */
	
	public function addHtmlForm($array_form) {
		if (isset($array_form["name"])) {
			$this->html_forms[$array_form["name"]] = $array_form;
		} else {
			//Store but unreachable
			$this->html_forms[] = $array_form;
		}
	}
	
/** Generate a delete form in HTML
* @param id {int} largeur
* @return {string} HTML form
**/
	public function getHtmlDeleteForm($id = 0) {
		$html = "";
		$html .= '<form action="delete.php" method="post" class="inline_block">';
		
		//submit button
		$html .= '<input type="submit" id="submit_'.$this->name.'" name="submit_'.$this->name.'" value="Delete" />';
		
		//hidden class
		$html .= '<input type="hidden" id="data_class" name="data_class" value ="'.$this->name.'" />';
		
		//hidden ID
		$html .= '<input type="hidden" id="data_id" name="data_id" value="'.$id.'" />';
		
		//hidden page source
		$html .= '<input type="hidden" id="source_page" name="source_page" value="'.$_SERVER['REQUEST_URI'].'" />';
		
		$html .= '</form>';
		
		return $html;
	}
	
	/* BASIC HTML FORM */
	//From attr value
	
	//Form to add, edit
	public function getHtmlForm($name = "") {
		$html_form = new Html_form();
		$html_form->setHyperClass($this);
		
		foreach ($this->attr as $attr) {
			if (!($attr->flags & HIDDEN_FORM)) {
				if (!empty($name)) {
					//There is a form selected
					if (isset($this->html_forms[$name])) {
						if (in_array($attr->name, $this->html_forms[$name]["attr"])) {
							$html_form->addField($attr->getHtml(), $attr->form_style, $attr->getClassLabel($this->name));
						}
					} else {
						//Stop foreach loop
						break;
					}
				} else {
					$html_form->addField($attr->getHtml(), $attr->form_style, $attr->getClassLabel($this->name));
				}
			}
		}
		
		return $html_form->generateHtml();
	}
	
	/* HTML TABLE */
	
	public function addHtmlTable($array_table) {
		if (isset($array_table["name"])) {
			$this->html_tables[$array_table["name"]] = $array_table;
		} else {
			//Store but unreachable
			$this->html_tables[] = $array_table;
		}
	}
	
	//Array
	//From data
	public function getHtmlArray($name = "") {
		return $this->getHtmlArrayNew($name);
	}
	
	public function getHtmlArrayOld() {
		$primary_key = $this->getPrimaryKey()->name;
		
		$html = "";
		
		$html .= '<a href="form.php?data_class='.$this->name.'">'."New".'</a>';
		$html .= '<table>';
		
		//Header
		$html .= '<thead>';
		$html .= '<tr>';
		
		foreach ($this->attr as $attr) {
			if (!($attr->flags & HIDDEN_TABLE)) {
				$html .= '<th>';
				//label
				$html .= $GLOBALS['project']->language->getClass($this->name, $attr->name);
				$html .= '</th>';
			}
		}
		$html .= '<th>'.$GLOBALS['project']->language->get("action").'</th>';
		$html .= '</tr>';
		$html .= '</thead>';
		
		//Body
		if (count($this->data)) {
			$html .= '<tbody>';
			
			foreach ($this->data as $data) {
				$html .= '<tr>';
				
				foreach ($this->attr as $attr) {
					if (!($attr->flags & HIDDEN_TABLE)) {
						$html .= '<td>';
						if (isset($data[$attr->name])) {
							$html .= $data[$attr->name];
						}
						$html .= '</td>';
					}
				}
				
				//Actions
				$html .= '<td>';
				$html .= '<a href="form.php?data_class='.$this->name.'&data_id='.$data[$primary_key].'">'."Edit".'</a>';
				$html .= $this->getHtmlDeleteForm($data[$primary_key]);
				$html .= '</td>';
				
				$html .= '</tr>';
			}
			
			$html .= '</tbody>';
		}
		
		//Footer
		$html .= '<tfoot>';
		$html .= '<tr>';
		
		foreach ($this->attr as $attr) {
			if (!($attr->flags & HIDDEN_TABLE)) {
				$html .= '<th>';
				//label
				$html .= $GLOBALS['project']->language->getClass($this->name, $attr->name);
				$html .= '</th>';
			}
		}
		$html .= '<th>'.$GLOBALS['project']->language->get("action").'</th>';
		$html .= '</tr>';
		$html .= '</tfoot>';
		
		$html .= '</table>';
		
		return $html;
	}
	
	public function getHtmlArrayNew($name = "") {
		$html_table = new Html_table();
		$html_table->setHyperClass($this);
		
		foreach ($this->attr as $attr) {
			if (!($attr->flags & HIDDEN_TABLE)) {
				if (!empty($name)) {
					//There is a table model as parameter
					if (isset($this->html_tables[$name])) {
						if (in_array($attr->name, $this->html_tables[$name]["attr"])) {
							$html_table->addColumn($attr);
						}
					} else {
						//Stop foreach loop
						break;
					}
				} else {
					$html_table->addColumn($attr);
				}
			}
		}
		
		return $html_table->generateHtml();
	}
	
	public function getHtmlButtonNew() {
		return '<a href="form.php?data_class='.$this->name.'">'."New".'</a>';
	}
}