<?php

require_once "class/hyperobject_plugin.php";

class PasswordProtect extends HyperObjectPlugin {
	private $login;
	private $password;
	
	public function __construct() {
		$this->constructor();
		$this->addAttr(new HyperAttribute_String("PasswordProtect_login"));
		$this->addAttr(new HyperAttribute_Password("PasswordProtect_password"));
		
		$this->login = "afpa";
		$this->password = "pwd";
		
		$GLOBALS['project']->addHook("beforeRender", "PasswordProtect", "plugin");
	}
	
	/*public function generateHtml() {
		$html_form = new Html_form();
		
		$html_form->addField('<input type="text" id="PasswordProtect_login" name="PasswordProtect_login" placeholder="Login" value="" />', "STYLE_LABEL_INPUT", '<label for="PasswordProtect_login">Login</label>');
		$html_form->addField('<input type="password" id="PasswordProtect_password" name="PasswordProtect_password" placeholder="Password" value="" />', "STYLE_LABEL_INPUT", '<label for="PasswordProtect_password">Password</label>');
		
		return $html_form->generateHtml();
	}*/
	
	public function plugin() {
		$result = TRUE;
		if (isset($_SESSION["PasswordProtect_checkLogin"])) {
			if ($_SESSION["PasswordProtect_checkLogin"]) {
				//ok, proceed
				return $result;
			} else {
				$result = FALSE;
			}
		} else {
			$result = FALSE;
		}
		
		if ((isset($_POST['PasswordProtect_login'])) && (isset($_POST['PasswordProtect_password']))) {
			if (($_POST['PasswordProtect_login'] == $this->login) && ($_POST['PasswordProtect_password'] == $this->password)) {
				$_SESSION["PasswordProtect_checkLogin"] = TRUE;
				$result = TRUE;
				return $result;
			}
		}
		
		if (!$result) {
			return $this->getHtmlForm();
		}
	}
}