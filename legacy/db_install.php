<?php

$req_array = array();

/* QUERIES */

$req_t_account = "CREATE TABLE t_account (
id_acc INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
acc_email VARCHAR (128) NOT NULL,
acc_pwd CHAR (32) NOT NULL,
acc_name VARCHAR (128) NOT NULL,
acc_superadmin INT (1) UNSIGNED NOT NULL,
acc_remme TEXT NOT NULL,
acc_doj DATE NOT NULL,
acc_active INT (1) UNSIGNED NOT NULL,
acc_lastvisit INT (11) UNSIGNED NOT NULL,
acc_delete INT (1) UNSIGNED NOT NULL,
PRIMARY KEY (id_acc),
UNIQUE (acc_email)
)";
array_push($req_array,$req_t_account);

$req_t_log_account = "CREATE TABLE t_log_account (
id_log INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_acc INT (11) UNSIGNED NOT NULL,
log_ip CHAR(15) NOT NULL,
log_remmetoken CHAR(10) NOT NULL,
log_page VARCHAR(255) NOT NULL,
log_date INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_log),
FOREIGN KEY (id_acc) REFERENCES t_account(id_acc)
)";
array_push($req_array, $req_t_log_account);

$req_t_api = "CREATE TABLE t_api (
id_api INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
api_name VARCHAR (255) NOT NULL,
api_dbname VARCHAR (32) NOT NULL,
api_desc TEXT NOT NULL,
api_private INT (1) UNSIGNED NOT NULL,
api_delete INT (1) UNSIGNED NOT NULL,
PRIMARY KEY (id_api),
UNIQUE (api_dbname)
)";
array_push($req_array,$req_t_api);

$req_t_app = "CREATE TABLE t_app (
id_app INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_api INT (11) UNSIGNED DEFAULT NULL,
app_name VARCHAR (128) NOT NULL,
app_dbname VARCHAR (32) NOT NULL,
app_createdate INT (11) UNSIGNED NOT NULL,
app_template INT (1) UNSIGNED NOT NULL,
app_delete INT (1) UNSIGNED NOT NULL,
app_webview INT (1) UNSIGNED NOT NULL,
app_backoffice_webview INT (1) UNSIGNED NOT NULL,
app_store_title VARCHAR (64) NOT NULL,
app_store_desc TEXT NOT NULL,
app_store_url TEXT NOT NULL,
PRIMARY KEY (id_app),
FOREIGN KEY (id_api) REFERENCES t_api(id_api),
UNIQUE (app_dbname)
)";
array_push($req_array,$req_t_app);

$req_t_account_app = "CREATE TABLE t_account_app (
id_acc_app INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_acc INT (11) UNSIGNED NOT NULL,
id_app INT (11) UNSIGNED NOT NULL,
acc_app_owner INT (1) UNSIGNED NOT NULL,
acc_app_admin INT (1) UNSIGNED NOT NULL,
acc_app_writer INT (1) UNSIGNED NOT NULL,
PRIMARY KEY (id_acc_app),
FOREIGN KEY (id_acc) REFERENCES t_account(id_acc),
FOREIGN KEY (id_app) REFERENCES t_app(id_app)
)";
array_push($req_array,$req_t_account_app);

$req_t_module = "CREATE TABLE t_module (
id_mod INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
mod_name VARCHAR (128) NOT NULL,
mod_dbname VARCHAR (32) NOT NULL,
mod_desc TEXT NOT NULL,
mod_private INT (1) UNSIGNED NOT NULL,
mod_delete INT (1) UNSIGNED NOT NULL,
mod_level INT (1) UNSIGNED NOT NULL,
mod_global INT (1) UNSIGNED NOT NULL,
date_create INT(11) UNSIGNED NOT NULL,
date_edit INT(11) UNSIGNED NOT NULL,
PRIMARY KEY (id_mod),
UNIQUE (mod_dbname)
)";
array_push($req_array,$req_t_module);

$req_t_app_module = "CREATE TABLE t_app_module (
id_app_mod INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_app INT (11) UNSIGNED NOT NULL,
id_mod INT (11) UNSIGNED NOT NULL,
app_mod_order INT (1) UNSIGNED NOT NULL,
app_mod_cron TEXT NOT NULL,
PRIMARY KEY (id_app_mod),
FOREIGN KEY (id_app) REFERENCES t_app(id_app),
FOREIGN KEY (id_mod) REFERENCES t_module(id_mod)
)";
array_push($req_array,$req_t_app_module);

$req_t_theme = "CREATE TABLE t_theme (
id_theme INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
theme_name VARCHAR (128) NOT NULL,
theme_filename VARCHAR (32) NOT NULL,
theme_desc TEXT NOT NULL,
theme_private INT (1) UNSIGNED NOT NULL,
theme_delete INT (1) UNSIGNED NOT NULL,
date_create INT (11) UNSIGNED NOT NULL,
date_edit INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_theme),
UNIQUE (theme_filename)
)";
array_push($req_array,$req_t_theme);

$req_t_menu_structure = "CREATE TABLE t_menu_structure (
id_menu_structure INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
structure_name VARCHAR (128) NOT NULL,
structure_filename VARCHAR (32) NOT NULL,
structure_desc TEXT NOT NULL,
structure_template TEXT NOT NULL,
structure_header INT (1) UNSIGNED NOT NULL,
structure_fooder INT (1) UNSIGNED NOT NULL,
structure_private INT (1) UNSIGNED NOT NULL,
structure_delete INT (1) UNSIGNED NOT NULL,
date_create INT (11) UNSIGNED NOT NULL,
date_edit INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_menu_structure),
UNIQUE (structure_filename)
)";
array_push($req_array,$req_t_menu_structure);

$req_t_os = "CREATE TABLE t_os (
id_os INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
os_code CHAR (3) NOT NULL,
os_name VARCHAR (16) NOT NULL,
PRIMARY KEY (id_os),
UNIQUE (os_code)
)";
array_push($req_array,$req_t_os);

$req_t_icon = "CREATE TABLE t_icon (
id_icon INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
icon_name VARCHAR (64) NOT NULL,
icon_dir VARCHAR (64) NOT NULL,
icon_width INT (8) UNSIGNED NOT NULL,
icon_height INT (8) UNSIGNED NOT NULL,
os_code CHAR (3) NOT NULL,
PRIMARY KEY (id_icon),
FOREIGN KEY (os_code) REFERENCES t_os(os_code)
)";
array_push($req_array,$req_t_icon);

$req_t_screen = "CREATE TABLE t_screen (
id_screen INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
screen_name VARCHAR (64) NOT NULL,
screen_dir VARCHAR (64) NOT NULL,
screen_width INT (8) UNSIGNED NOT NULL,
screen_height INT (8) UNSIGNED NOT NULL,
screen_landscape INT (1) UNSIGNED NOT NULL DEFAULT '0',
screen_ratio FLOAT NOT NULL,
os_code CHAR (3) NOT NULL,
PRIMARY KEY (id_screen),
FOREIGN KEY (os_code) REFERENCES t_os(os_code)
)";
array_push($req_array,$req_t_screen);

$req_t_plugin = "CREATE TABLE t_plugin (
id_plugin INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
plugin_name VARCHAR (255) NOT NULL,
plugin_npmpackage VARCHAR (255) NOT NULL,
plugin_git VARCHAR (255) NOT NULL,
plugin_os INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_plugin),
UNIQUE (plugin_npmpackage)
)";
array_push($req_array,$req_t_plugin);

$req_t_app_plugin = "CREATE TABLE t_app_plugin (
id_app_plugin INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_app INT (11) UNSIGNED NOT NULL,
id_plugin INT (11) UNSIGNED NOT NULL,
param TEXT NOT NULL,
xml TEXT NOT NULL,
PRIMARY KEY (id_app_plugin),
FOREIGN KEY (id_app) REFERENCES t_app(id_app),
FOREIGN KEY (id_plugin) REFERENCES t_plugin(id_plugin)
)";
array_push($req_array,$req_t_app_plugin);

$req_t_mod_plugin = "CREATE TABLE t_mod_plugin (
id_mod_plugin INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_mod INT (11) UNSIGNED NOT NULL,
id_plugin INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_mod_plugin),
FOREIGN KEY (id_mod) REFERENCES t_module(id_mod),
FOREIGN KEY (id_plugin) REFERENCES t_plugin(id_plugin)
)";
array_push($req_array,$req_t_mod_plugin);

/* END QUERIES */

/* CONTENT QUERIES */

$req_array_content = array();

array_push($req_array_content,'INSERT INTO t_os VALUES (NULL,"and","Android")');
array_push($req_array_content,'INSERT INTO t_os VALUES (NULL,"ios","iOS")');

//IOS ICONS
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","icons",57,57,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon@2x.png","icons",114,114,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-40.png","icons",40,40,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-40@2x.png","icons",80,80,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-50.png","icons",50,50,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-50@2x.png","icons",100,100,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-60.png","icons",60,60,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-60@2x.png","icons",120,120,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-60@3x.png","icons",180,180,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-72.png","icons",72,72,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-72@2x.png","icons",144,144,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-76.png","icons",76,76,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-76@2x.png","icons",152,152,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-small.png","icons",29,29,"ios")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon-small@2x.png","icons",58,58,"ios")');

//ANDROID ICONS
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable",96,96,"and")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable-ldpi",36,36,"and")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable-mdpi",48,48,"and")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable-hdpi",72,72,"and")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable-xhdpi",96,96,"and")');
array_push($req_array_content,'INSERT INTO t_icon VALUES (NULL,"icon.png","drawable-xxhdpi",144,144,"and")');

//IOS SCREENS
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default~iphone.png","splash",320,480,0,0.6,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default@2x~iphone.png","splash",640,960,0,0.666,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-568h@2x~iphone.png","splash",640,1136,0,0.5625,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-Portrait~ipad.png","splash",768,1024,0,0.75,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-Portrait@2x~ipad.png","splash",1536,2048,0,0.75,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-Landscape~ipad.png","splash",1024,768,1,0.75,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-Landscape@2x~ipad.png","splash",2048,1536,1,0.75,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-667h.png","splash",750,1334,0,0.5625,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-736h.png","splash",1242,2208,0,0.5625,"ios")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"Default-Landscape-736h.png","splash",2208,1242,1,0.5625,"ios")');

//ANDROID SCREENS
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"screen.png","drawable",720,1280,0,0.5625,"and")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"screen.png","drawable-port-ldpi",200,320,0,0.6,"and")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"screen.png","drawable-port-mdpi",320,480,0,0.666,"and")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"screen.png","drawable-port-hdpi",480,800,0,0.6,"and")');
array_push($req_array_content,'INSERT INTO t_screen VALUES (NULL,"screen.png","drawable-port-xhdpi",720,1280,0,0.5625,"and")');

/* END QUERIES */

/* FILL DB */

$req_array_content = array();

//Connect
include 'db_connect.php';

//Tables creation
foreach ($req_array as $req_query) {
	$link->query($req_query) or die(mysqli_error($link));
}

//Content creation
foreach ($req_array_content as $req_query) {
	$link->query($req_query) or die(mysqli_error($link));
}

//Close connection
mysqli_close($link);

echo "done";

?>