<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
		<meta name="author" content="ABoeglin" />
		<title><?php echo MyProject::$name; ?></title>
		<link rel="stylesheet" href="js/select2/select2.min.css" />
		<link rel="stylesheet" href="css/css.css" />
		<link rel="icon" href="favicon.ico" />
		
		<link rel="stylesheet" href="css/icomoon/icomoon.css" />
		<link rel="stylesheet" href="css/font-awesome/font-awesome.css" />
		<link rel="stylesheet" href="css/Metrize-Icons/metrize.css" />
		<link rel="stylesheet" href="css/ionicons/ionicons.css" />
		<link rel="stylesheet" href="css/picol/picol.css" />
		<link rel="stylesheet" href="css/moby-icons/moby-icons.css" />

		<script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		
		<link rel="stylesheet" href="js/jquery-ui-1.11.4/jquery-ui.min.css" />
		<link rel="stylesheet" href="js/jquery-ui-1.11.4/jquery-ui.theme.min.css" />
		<script language="javascript" type="text/javascript" src="js/jquery-ui-1.11.4/jquery-ui.min.js"></script>

		<link rel="stylesheet" href="js/image-picker/image-picker.css" />
		<script language="javascript" type="text/javascript" src="js/image-picker/image-picker.min.js"></script>

		<link rel="stylesheet" href="js/remodal/remodal.css">
		<link rel="stylesheet" href="js/remodal/remodal-default-theme.css">
		<script src="js/remodal/remodal.min.js"></script>
		
		<script src="js/tinymce/tinymce.min.js"></script>
		
		<script src="js/jQueryColorPicker.min.js"></script>
		
		<script type="text/javascript" src="js/select2/select2.min.js"></script>

		<script type="text/javascript" src="js/ace/src-min-noconflict/ace.js" charset="utf-8"></script>
		
		<script language="javascript" type="text/javascript" src="js/functions.js"></script>
    </head>
	<body>
		<div id="wrapper">