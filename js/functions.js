function recalcColor(element_target) {
	var strres=""; //rgba(0, 0, 255, 0.5)
	var colors = $('#'+element_target+'_color').val();
	var opacity = $('#'+element_target+'_opacity').val();
	if (opacity>100) {
		opacity=100;
	}
	if (opacity<0) {
		opacity=0;
	}
	opacity=opacity/100;
	
	var valred = parseInt("0x"+colors.substr(1,2));
	var valgreen = parseInt("0x"+colors.substr(3,2));
	var valblue = parseInt("0x"+colors.substr(5,2));
	
	strres="rgba("+valred+", "+valgreen+", "+valblue+", "+opacity+")";
	$('#'+element_target).val(strres);
}