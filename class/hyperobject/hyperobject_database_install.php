<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperobject_database.php";

abstract class HyperObjectDatabaseInstall extends HyperObjectDatabase {
	public $database_table; //Object based on attr, with SQL database structure
	
	public function __construct() {
		parent::__construct();
        $this->database_table = new DatabaseTable();
		$this->database_table->name = $this->name;
    }
	
/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
		
		$this->data = array();
		$this->data_ptr = null;
		
		$this->html_tables = array();
		$this->html_forms = array();
		
        $this->database_table = new DatabaseTable();
		$this->database_table->name = $this->name;
	}
	
	
/** Initialize, load attributes
**/
	public function init() {
		$this->database_table->loadAttrs($this->attr);
	}
	
	/* INSTALL */
	
/** Create table
* @return {string} Query result
**/
	public function install() {
		return $GLOBALS['project']->database_connexion->db_query($this->database_table->getString());
	}

/** Drop table
* @return {string} Query result
**/
	public static function uninstall() {
		$table = new DatabaseTable();
		$table->loadString(static::$query);
		
		$query = "DROP TABLE ".$table->name;
		return $GLOBALS['project']->database_connexion->db_query($query);
	}
	
	public function install_checkDbTable() {
		$query = "SHOW TABLES LIKE '".$this->name."'";
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		return $result->num_rows;
	}
	
	public function install_checkDbCols() {
		//$result = array();
		$result = new Database_check();
		
		if (!$this->install_checkDbTable()) {
			//$result[] = "Table ".$this->name." doesnt exist";
			$result->addTable($this->name, DBCHECK_ERROR_MISSING);
		} else {
			$result->addTable($this->name, DBCHECK_OK);
			foreach ($this->attr as $attr) {
				if (!$attr->install_checkDbCol($this->name)) {
					//$result[] = "Column ".$attr->name." doesnt exist";
					$result->addColumn($attr->name, $this->name, DBCHECK_ERROR_MISSING);
				} else {
					$result->addColumn($attr->name, $this->name, DBCHECK_OK);
				}
			}
		}
		return $result;
	}
	
	public function install_col($col) {
		return $GLOBALS['project']->database_connexion->db_query($this->database_table->getColString($col));
	}
}