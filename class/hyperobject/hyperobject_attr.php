<?php

/**********
***********
**********/
/* HYPER OBJECT */
/**********
***********
**********/

require_once "hyperattribute.php";

abstract class HyperObjectAttr {
    public $name; //Class name
	public $attr; //Array of HyperAttribute
	
    public function __construct() {
		parent::__construct();
		$this->name = get_class($this);
		$this->attr = array();
    }

/** Constructor
**/
	public function constructor() {
		$this->name = get_class($this);
		$this->attr = array();
	}
	
	/* ATTRIBUTES */
	
/** Add attribute to object array
**/
	public function addAttr($attr) {
		$this->attr[$attr->name] = $attr;
	}
	
	public function searchAttr($type, $multiple_result = FALSE) {
		$result = FALSE;
		if ($multiple_result) {
			$result = array();
		}
		
		foreach ($this->attr as $attr) {
			if ($attr->type === $type) {
				if ($multiple_result) {
					$result[] = $attr;
				} else {
					$result = $attr;
					break;
				}
			}
		}
		
		return $result;
	}
	
	public function getPrimaryKey() {
		foreach ($this->attr as $attr) {
			if ($attr->type === "id") {
				return $attr;
			}
		}
		return NULL;
	}
}