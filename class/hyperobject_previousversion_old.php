<?php

require_once "project.php";

abstract class HyperObject {
	public static $query = "";
    public $name;
	public $database_table;
	public $data;
	
    public function __construct() {
		$this->name = get_class($this);
        $this->database_table = new DatabaseTable();
        $this->database_table->loadString(static::$query);
		$this->data = array();
    }
	
	//Form to add, edit
	public function getHtmlForm($id = 0) {
		$html = "";
		$html .= '<form action="" method="post">';
		
		foreach ($this->database_table->columns as $column) {
			$html .= '<div>';
			switch ($column->type) {
				case "INT":
					if (intval($column->size)==1) {
						$html .= '<input type="checkbox" name="'.$column->name.'"';
						if ($id) {
							//$html .= ' value=""';
						}
						$html .= ' />';
					} else {
						$html .= '<input type="number" name="'.$column->name.'" placeholder="'.$column->name.'"';
						if ($id) {
							$html .= ' value=""';
						}
						$html .= ' />';
					}
					break;
				case "TEXT":
					$html .= '<textarea name="'.$column->name.'" placeholder="'.$column->name.'">';
					if ($id) {
						$html .= '';
					}
					$html .= '</textarea>';
					break;
				case "DATE":
					$html .= '<input type="date" name="'.$column->name.'" placeholder="'.$column->name.'"';
					if ($id) {
						$html .= ' value=""';
					}
					$html .= ' />';
					break;
				case "DATETIME":
					$html .= '<input type="datetime-local" name="'.$column->name.'" placeholder="'.$column->name.'"';
					if ($id) {
						$html .= ' value=""';
					}
					$html .= ' />';
					break;
				default:
					$html .= '<input type="text" name="'.$column->name.'" placeholder="'.$column->name.'"';
					if ($id) {
						$html .= ' value=""';
					}
					$html .= ' />';
			}
			$html .= '</div>';
		}
		
		if ($id) {
			//edit
			$html .= '<input type="hidden" name="action" value="edit" />';
		} else {
			//new
			$html .= '<input type="hidden" name="action" value="new" />';
		}
		$html .= '<input type="submit" id="" name="" />';
		$html .= '</form>';
		
		return $html;
	}
	
	public function catchPost() {
		
	}
	
	//Array
	public function getHtmlArray() {
		$html = "";
		
		$html .= '<table>';
		//Header
		$html .= '<thead>';
		$html .= '<tr>';
		foreach ($this->database_table->columns as $column) {
			$html .= '<th>';
			$html .= $column->name;
			$html .= '</th>';
		}
		$html .= '</tr>';
		$html .= '</thead>';
		
		//Body
		$html .= '<tbody>';
		foreach ($this->data as $data) {
			$html .= '<tr>';
			foreach ($this->database_table->columns as $column) {
				$html .= '<td>';
				$html .= $data[$column->name];
				$html .= '</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		
		//Footer
		$html .= '<tfoot>';
		$html .= '<tr>';
		foreach ($this->database_table->columns as $column) {
			$html .= '<th>';
			$html .= $column->name;
			$html .= '</th>';
		}
		$html .= '</tr>';
		$html .= '</tfoot>';
		$html .= '</table>';
		
		return $html;
	}
	
	//Install
	public static function install() {
		return $GLOBALS['project']->database_connexion->db_query(static::$query);
	}
	
	public static function uninstall() {
		$table = new DatabaseTable();
		$table->loadString(static::$query);
		
		$query = "DROP TABLE ".$table->name;
		return $GLOBALS['project']->database_connexion->db_query($query);
	}
	
	//Setter and Getter
	public function get($attr = "") {
		
	}
	
	public function set($attr = "", $val = "") {
		$i_column = $this->database_table->findColumnByName($attr);
		if ($i_column !== false) {
			switch ($this->database_table->columns[$i_column]->type) {
				default:
					return $this->database_table->columns[$i_column]->type;
			}
		}
	}
	
	//Get multiple objects
	public function loadAll() {
		$query = "SELECT * FROM ".$this->database_table->name;
		$result = $GLOBALS['project']->database_connexion->db_query($query);
		
		while ($row = $result->fetch_array()) {
			$this->data[] = $row;
		}
	}
	
	//Get single object
	public function load($key) {
		
	}
}
?>
