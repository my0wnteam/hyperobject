<?php
//Initialize everything needed

/* REQUIRE */
require_once "./config.php";
require_once "./functions.php";

/* SESSION */
session_name(CONF_SESSION_NAME);
session_start();

require_once "./myproject.php";

/* LOG */

//ini_set are used to overwrite PHP config.ini
ini_set("log_errors", CONF_ERROR_BOOL);
ini_set("error_log", CONF_ERROR_FILE);

/* PROJECT SINGLETON */
$GLOBALS['project'] = new MyProject();
//$GLOBALS['project']->uninstall();
//$GLOBALS['project']->install();