<?php

include_once "lang/class/user.php";

$GLOBALS['LANG']['datetime_create'] = "Create date";
$GLOBALS['LANG']['datetime_edit'] = "Edit date";
$GLOBALS['LANG']['actions'] = "Actions";
$GLOBALS['LANG']['yes'] = "Yes";
$GLOBALS['LANG']['no'] = "No";
$GLOBALS['LANG']['new'] = "New";
$GLOBALS['LANG']['any'] = "Any";
$GLOBALS['LANG']['filter'] = "Filter";
$GLOBALS['LANG']['edit'] = "Edit";
$GLOBALS['LANG']['delete'] = "Delete";
$GLOBALS['LANG']['save'] = "Save";
$GLOBALS['LANG']['back'] = "Back";
$GLOBALS['LANG']['table'] = "Table";
$GLOBALS['LANG']['form'] = "Form";