<?php

define("DBCHECK_OK", 0);
define("DBCHECK_ERROR_MISSING", 1);

class Database_check {
	public $tables;
	
    public function __construct() {
		$this->tables = array();
    }
	
	private function __clone() {}
	
	public static function errorMessage($error_code) {
		switch($error_code) {
			case DBCHECK_OK:
				return "Exists";
				break;
			case DBCHECK_ERROR_MISSING:
				return "Doesnt exist";
				break;
		}
	}
	
	public function addTable($table, $code) {
		$this->tables[$table]["columns"] = array();
		$this->tables[$table]["code"] = $code;
	}
	
	public function addColumn($column, $table, $code) {
		$this->tables[$table]["columns"][$column] = $code;
	}
}
?>