<?php 
include 'init.php';

//Redirect server side
//header("Location: ".$_SERVER['REQUEST_URI']);
//die;

if (isset($_POST)) {
	if (isset($_POST['source_page'])) {
		if (isset($_POST['data_class'])) {
			if (isset($_POST['data_id'])) {
				$source_page = clearFormInput($_POST['source_page']);
				$data_class = clearFormInput($_POST['data_class']);
				$data_id = clearFormInput($_POST['data_id']);
				
				//create class
				$post_class = new $data_class();
				
				//delete
				$post_class->deleteData($data_id);
				
				//return to source
				header("Location: ".basename(htmlspecialchars_decode($source_page)));
				die;
			}
		}
	}
}

if (isset($_GET['source_page'])) {
	if (isset($_GET['data_class'])) {
		if (isset($_GET['data_id'])) {
			$source_page = clearFormInput($_GET['source_page']);
			$data_class = clearFormInput($_GET['data_class']);
			$data_id = clearFormInput($_GET['data_id']);
			
			//create class
			$post_class = new $data_class();
			
			//delete
			$post_class->deleteData($data_id);
			
			//return to source
			header("Location: ".basename(htmlspecialchars_decode($source_page)));
			die;
		}
	}
}

?>
<?php $GLOBALS['project']->database_connexion->db_close(); ?>