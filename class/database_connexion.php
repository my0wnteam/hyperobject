<?php

class Database_connexion {
	private static $db;
	public $pdo;

    public function __construct() {
        $this->pdo = 0;
    }
	
	private function __clone() {}
	
	//OPEN DATABASE
	
	public function db_connect($db_name) {
		if ($this->pdo) {
			$this->db_connect_pdo($db_name);
		} else {
			$this->db_connect_mysqli($db_name);
		}
	}
	
	function db_connect_mysqli($db_name) {
		self::$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, $db_name);
	}
	
	function db_connect_pdo($db_name) {
		try {
			self::$db = new PDO($this->db_connect_pdo_dsn(DB_HOST, $db_name, DB_CHARSET), DB_USER, DB_PASSWORD);
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
	
	function db_connect_pdo_dsn($db_host, $db_name, $db_charset="") {
		//mysql:host=;dbname=;charset=
		$dsn = "mysql:host=".$db_host.";dbname=".$db_name;
		if (!empty($db_charset)) {
			$dsn .= ";charset=".$db_charset;
		}
		return $dsn;
	}
	
	//CLOSE DATABASE
	public function db_close() {
		if ($this->pdo) {
			self::$db = null;
		} else {
			self::$db->close();
		}
	}
	
	//SELECT DATABASE
	public function db_select($db_name) {
		if ($this->pdo) {
			return $this->db_query("USE ".$db_name);
		} else {
			return self::$db->select_db($db_name);
		}
	}
	
	//QUERY DATABASE
	public function db_query($query, $args = array()) {
		if ($this->pdo) {
			$stmt = self::$db->prepare($query);			
			if (!empty($args)) {
				foreach ($args as $index=>$arg) {
					$stmt->bindValue($index, $arg);
				}
			}
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} else {
			if (!empty($args)) {
				foreach ($args as $arg) {
					if (!is_numeric($arg)) {
						$arg = "'".self::$db->real_escape_string($arg)."'";
					}
					$query = preg_replace("/".preg_quote("?")."/", $arg, $query, 1);
				}
			}
			$result = self::$db->query($query);
		}
		
		return $result;
	}
	
	//TRANSACTION
	public function db_transaction($autocommit=TRUE) {
		if ($this->pdo) {
			self::$db->beginTransaction($autocommit);
		} else {
			self::$db->autocommit($autocommit);
		}
	}
	
	//COMMIT TRANSACTION
	public function db_commit() {
		if ($this->pdo) {
			self::$db->commit();
		} else {
			self::$db->commit();
		}
	}
	
	//GET ERROR
	public function db_error() {
		return self::$db->error;
	}
}
?>