<?php

$GLOBALS['LANG']['USER']['id_acc'] = "ID";
$GLOBALS['LANG']['USER']['acc_email'] = "E-mail";
$GLOBALS['LANG']['USER']['acc_pwd'] = "Password";
$GLOBALS['LANG']['USER']['acc_name'] = "Name";
$GLOBALS['LANG']['USER']['acc_fname'] = "First name";
$GLOBALS['LANG']['USER']['acc_superadmin'] = "Admin";
$GLOBALS['LANG']['USER']['acc_token'] = "Token";
$GLOBALS['LANG']['USER']['acc_active'] = "Active";
$GLOBALS['LANG']['USER']['acc_delete'] = "Delete";
$GLOBALS['LANG']['USER']['acc_lastvisit'] = "Last visit";