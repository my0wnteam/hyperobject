<?php 
include 'init.php';

//Redirect server side
//header("Location: ".$_SERVER['REQUEST_URI']);
//die;

if (isset($_POST)) {
	if (isset($_POST['source_page'])) {
		
		$source_page = clearFormInput($_POST['source_page']);
		if ($source_page === "/" || empty($source_page)) {
			$source_page = "index.php";
		}
		
		if (isset($_POST['data_class'])) {
			$data_class = clearFormInput($_POST['data_class']);
			//memorize post data
			$_SESSION['post_data'] = $_POST;
			
			//create class
			$post_class = new $data_class();
			
			//catch post data
			$post_class->catchPost();
			
			//check errors
			if (!$post_class->errorCheck()) {
				//no errors
				
				//entity
				if (method_exists($post_class, "save")) {
					//save attr to data
					$_SESSION['result'] = $post_class->save();
					if ($_SESSION['result'] !== true) {
						echo $_SESSION['result'];
						//die;
					}
				}
				
				//plugin
				if (method_exists($post_class, "plugin")) {
					$post_class->plugin();
				}
			}
		}
		
		//return to source
		header("Location: ".basename(htmlspecialchars_decode($source_page)));
		//header("Location: index.php");
		die;
	}
}

$GLOBALS['project']->database_connexion->db_close();